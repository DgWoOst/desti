'**************************************************************************
' P&G BM007 M20_St10
'**************************************************************************
' Roboter					G10-854S
' Serial No. 				GA03013145
' Manufactured				02/2018
'
' Controller				RC700-A
' Serial No.				R7GL015192
' Manufactured				12/2017
'**************************************************************************
' 1.0		10.10.2018		eikotec   		Creation
' 1.1
'**************************************************************************

   

  
#include "settings.inc"


' Global Variables
Global Real MinRaiseFallDist(22)
Global String PosNames$(22)


Function main

	Print "Main"
	
'Call FC for Init Robot
	Print "Init"
	Call fcRobotInit()
	
	Call fcBoxCalc()
	
	Trap Pause Xqt fcPause
	
'Call Main Sequence
	Print "Main Sequence Start"
	Call fcMainSequence
	Print "Main Sequence End"
Fend




Function fcBoxCalc
		
	
	Print "Calculate boxes for module: " + Str$(MODULE_ID)
	
	If MODULE_ID = 20 Then

		' Define Box 1: 1-Signal -> Robot is outside of cylinder movement St10 RT_Pick
		Box 1, 1, (CX(p_RT_Pick) - 200), (CX(p_RT_Pick) + 200), (CY(p_RT_Pick) - 170), (CY(p_RT_Pick) + 100), (CZ(p_RT_Pick) - 430), (CZ(p_RT_Pick) + 145), Off
		
		' Define Box 2: 1-Signal -> Robot is outside of cylinder movement St260 RT_Place
		Box 2, 1, (CX(p_RT_Place) - 70), (CX(p_RT_Place) + 180), (CY(p_RT_Place) - 100), (CY(p_RT_Place) + 320), (CZ(p_RT_Place) - 430), (CZ(p_RT_Place) + 145), Off
	
		' Define Box 3: 1-Signal -> Robot is outside of cylinder movement WPC_Pick
		Box 3, 1, (CX(p_WPC_Pick) - 160), (CX(p_WPC_Pick) + 160), (CY(p_WPC_Pick) - 1000), (CY(p_WPC_Pick) + 90), (CZ(p_WPC_Pick) - 430), (CZ(p_WPC_Pick) + 20), Off
		
		' Define Box 4: 1-Signal -> Robot is outside of cylinder movement WPC_Place
		Box 4, 1, (CX(p_WPC_Place) - 160), (CX(p_WPC_Place) + 160), (CY(p_WPC_Place) - 160), (CY(p_WPC_Place) + 1000), (CZ(p_WPC_Place) - 430), (CZ(p_WPC_Place) + 40), Off
		
		' Define Box 9: 1-Signal -> Robot is outside of roundtable movement
		Box 9, 1, (CX(p_DROP_NOK) - 1), (CX(p_DROP_NOK) + 1), (CY(p_DROP_NOK) - 1), (CY(p_DROP_NOK) + 1), (CZ(p_DROP_NOK) - 10), (CZ(p_DROP_NOK) + 10), Off


	ElseIf MODULE_ID = 40 Or MODULE_ID = 50 Or MODULE_ID = 60 Then
				
		  ' Define Box 1: 1-Signal -> Robot is outside of cylinder movement St10 RT_Pick
			Box 1, 1, (CX(p_RT_Pick) - 200), (CX(p_RT_Pick) + 500), (CY(p_RT_Pick) - 500), (CY(p_RT_Pick) + 500), (CZ(p_RT_Pick) - 430), (CZ(p_RT_Pick) + 40), Off
			
			' Define Box 2: 1-Signal -> Robot is outside of cylinder movement St10 RT_Place
			Box 2, 1, (CX(p_RT_Place) - 100), (CX(p_RT_Place) + 500), (CY(p_RT_Place) - 300), (CY(p_RT_Place) + 300), (CZ(p_RT_Place) - 430), (CZ(p_RT_Place) + 40), Off
		
			' Define Box 3: 1-Signal -> Robot is outside of cylinder movement WPC_Pick
			Box 3, 1, (CX(p_WPC_Pick) - 160), (CX(p_WPC_Pick) + 160), (CY(p_WPC_Pick) - 1000), (CY(p_WPC_Pick) + 90), (CZ(p_WPC_Pick) - 430), (CZ(p_WPC_Pick) + 30), Off
			
			' Define Box 4: 1-Signal -> Robot is outside of cylinder movement WPC_Place
			Box 4, 1, (CX(p_WPC_Place) - 160), (CX(p_WPC_Place) + 160), (CY(p_WPC_Place) - 160), (CY(p_WPC_Place) + 1000), (CZ(p_WPC_Place) - 430), (CZ(p_WPC_Place) + 30), Off
		
		ElseIf MODULE_ID = 70 Then
		
			' Define Box 1: 1-Signal -> Robot is outside of cylinder movement St60
			Box 1, 1, (CX(p_RT_PICK) - 100), (CX(p_RT_PICK) + 100), (CY(p_RT_PICK) - 100), (CY(p_RT_PICK) + 100), (CZ(p_RT_PICK) - 430), (CZ(p_RT_PICK) + 50), Off
		
			' Define Box 2: 1-Signal -> Robot is outside of cylinder movement St10 RT_Place
			Box 2, 1, (CX(p_RT_Place) - 100), (CX(p_RT_Place) + 500), (CY(p_RT_Place) - 300), (CY(p_RT_Place) + 300), (CZ(p_RT_Place) - 430), (CZ(p_RT_Place) + 40), Off


			'Define Box 2: 1-Signal -> Robot is outside of cylinder movement WPC_Pick
			Box 3, 1, (CX(p_WPC_Pick) - 160), (CX(p_WPC_Pick) + 160), (CY(p_WPC_Pick) - 160), (CY(p_WPC_Pick) + 1000), (CZ(p_WPC_Pick) - 430), (CZ(p_WPC_Pick) + 40), Off
		
			' Define Box 3: 1-Signal -> Robot is outside of cylinder movement WPC_Place
			Box 4, 1, (CX(p_WPC_Place) - 160), (CX(p_WPC_Place) + 160), (CY(p_WPC_Place) - 1000), (CY(p_WPC_Place) + 90), (CZ(p_WPC_Place) - 430), (CZ(p_WPC_Place) + 40), Off
	
		
		Else
	
			
			Print "ERROR fcBoxCalc"
			Print " Quit all "
			Quit All
			
					
	EndIf
	



	' Define Box 5: 1-Signal -> Robot is at point p_RT_Pick
	Box 5, 1, (CX(p_RT_Pick) - 1), (CX(p_RT_Pick) + 1), (CY(p_RT_Pick) - 1), (CY(p_RT_Pick) + 1), (CZ(p_RT_Pick) - 1), (CZ(p_RT_Pick) + 1), On
	
	' Define Box 6: 1-Signal -> Robot is at point p_RT_Place
	Box 6, 1, (CX(p_RT_Place) - 1), (CX(p_RT_Place) + 1), (CY(p_RT_Place) - 1), (CY(p_RT_Place) + 1), (CZ(p_RT_Place) - 1), (CZ(p_RT_Place) + 1), On

	'Define Box 7: 1-Signal -> Robot is at point p_WPC	
	Box 7, 1, (CX(p_WPC_Pick) - 1), (CX(p_WPC_Pick) + 1), (CY(p_WPC_Pick) - 1), (CY(p_WPC_Pick) + 1), (CZ(p_WPC_Pick) - 1), (CZ(p_WPC_Pick) + 1), On
		
	'Define Box 8: 1-Signal -> Robot is at point p_WPC	
	Box 8, 1, (CX(p_WPC_Place) - 1), (CX(p_WPC_Place) + 1), (CY(p_WPC_Place) - 1), (CY(p_WPC_Place) + 1), (CZ(p_WPC_Place) - 1), (CZ(p_WPC_Place) + 1), On

	Integer BoxNr
	For BoxNr = 1 To 9
		If BoxDef(BoxNr) Then
			Print "Box", BoxNr, " succesful defined"
		Else
			Print "Box", BoxNr, " not succesful defined"
		EndIf
	Next
	
Fend


Function fcMainSequence

	Integer mStart_Prog
	Integer iProgNo
	
	Do
		
	
  	If Sw(Start_Prog) Then ' And Not mStart_Prog Then
    		
    	'Set Speed
    	Call fcPowerSpeed()
    	
    	Call fcBoxCalc()
		
			iProgNo = In(IN_Prog_Nr)
			
			If iProgNo >= 1 And iProgNo <= 22 Then
				
				'Init movement	
				Call fcInitPrg(iProgNo, PosNames$(iProgNo - 1))
				
				Call fcJumpToPoint(iProgNo - 1) 'Drive to Home Position
				
				'close movement
				Call fcEndPrg()
	
			EndIf
			
	  EndIf
	  
	  
	 ' mStart_Prog = Sw(Start_Prog)
	  
	 ' Wait 0.01
	
	Loop
	
Fend

Function fcJumpToPoint(iID_Target As Integer)
	
	Integer iID_Current
	Real LimZFrom, LimZTo, LimZEff
	

	'get the current position ID
	iID_Current = fcCurrentPointID()
	
	'if current position is undefined, homing is required
	If iID_Current = -1 And iID_Target <> PI_HOME Then
		On HomingRequired
		Print "HomingRequired"
		GoTo End_fcJumpToPoint
		
		
	ElseIf iID_Current = -1 And iID_Target = PI_HOME Then
	
	
		pTo = P(iID_Target)
		If CZ(pTo) > CZ(RealPos) Then
			Arch 0, CZ(RealPos) * (-1), 0
			LimZEff = 0
		Else
			Arch 0, 0, 0
			LimZEff = 0
		EndIf
	
	Else
		
			
		pFrom = P(iID_Current)
		pTo = P(iID_Target)
			
		Arch 0, MinRaiseFallDist(iID_Current), MinRaiseFallDist(iID_Target)
			
		LimZFrom = CZ(P(iID_Current)) + MinRaiseFallDist(iID_Current)
		LimZTo = CZ(P(iID_Target)) + MinRaiseFallDist(iID_Target)
		
		If (LimZFrom > LimZTo) And MinRaiseFallDist(iID_Current) = 0 Then
			LimZEff = CZ(pFrom)
		ElseIf (LimZFrom < LimZTo) And MinRaiseFallDist(iID_Target) = 0 Then
			LimZEff = CZ(pTo)
		ElseIf LimZFrom > LimZTo Then
			LimZEff = LimZFrom + CORNER_RADIUS
		Else
			LimZEff = LimZTo + CORNER_RADIUS
		EndIf
	
		If LimZEff > 0 Then
			LimZEff = 0
		EndIf
			
	EndIf
	
		
	If iID_Target = PI_DUMMY_PICK_1 Or iID_Target = PI_DUMMY_PICK_2 Or iID_Target = PI_DUMMY_PICK_3 Or iID_Target = PI_DUMMY_PICK_4 Or iID_Target = PI_DUMMY_PICK_5 Or iID_Target = PI_DUMMY_PICK_6 Then
	
		Jump HP_Dummy C0 LimZ CP
		Jump pTo C0 LimZ
		
	ElseIf iID_Target = PI_DUMMY_PLACE_1 Or iID_Target = PI_DUMMY_PLACE_2 Or iID_Target = PI_DUMMY_PLACE_3 Or iID_Target = PI_DUMMY_PLACE_4 Or iID_Target = PI_DUMMY_PLACE_5 Or iID_Target = PI_DUMMY_PLACE_6 Then
	
		Jump HP_Dummy C0 LimZ CP
		Jump pTo C0 LimZ
		
	ElseIf iID_Current = PI_DUMMY_PLACE_1 Or iID_Current = PI_DUMMY_PLACE_2 Or iID_Current = PI_DUMMY_PLACE_3 Or iID_Current = PI_DUMMY_PLACE_4 Or iID_Current = PI_DUMMY_PLACE_5 Or iID_Current = PI_DUMMY_PLACE_6 Then
	
		Jump HP_Dummy C0 LimZ CP
		Jump pTo C0 LimZ
		
	ElseIf iID_Current = PI_DUMMY_PICK_1 Or iID_Current = PI_DUMMY_PICK_2 Or iID_Current = PI_DUMMY_PICK_3 Or iID_Current = PI_DUMMY_PICK_4 Or iID_Current = PI_DUMMY_PICK_5 Or iID_Current = PI_DUMMY_PICK_6 Then
	
		Jump HP_Dummy C0 LimZ CP
		Jump pTo C0 LimZ
		
	ElseIf (iID_Current = PI_HOME And iID_Target = PI_DOOR) Or (iID_Current = PI_DOOR And iID_Target = PI_HOME) Then
		'From Home to DoorPos
		'From DoorPos to Home
		Jump HP_Dummy C0 LimZ CP
		Jump pTo C0 LimZ
		
	Else
		'default jump
		Jump pTo C0 LimZ LimZEff
		
EndIf
	
	WaitPos
	
	
	If iID_Target = PI_HOME Then
		On HomePosition
	EndIf
	
	
End_fcJumpToPoint:


	
Fend
	
	

Function fcRobotInit
	' Clear Command window text area
	Cls
	
	Print "fcRobotInit Start"
	
	'Fill global arrays with include defines (settings.inc)
	MinRaiseFallDist(PI_HOME) = MINRAISEFALLDIST_HOME
	MinRaiseFallDist(PI_WPC_PLACE) = MINRAISEFALLDIST_WPC_PLACE
	MinRaiseFallDist(PI_RT_PICK) = MINRAISEFALLDIST_RT_PICK
	MinRaiseFallDist(PI_WPC_PICK) = MINRAISEFALLDIST_WPC_PICK
	MinRaiseFallDist(PI_RT_PLACE) = MINRAISEFALLDIST_RT_PLACE
	MinRaiseFallDist(PI_DROP_NOK) = MINRAISEFALLDIST_DROP_NOK
	MinRaiseFallDist(PI_RT_SAVE) = MINRAISEFALLDIST_RT_SAVE
	MinRaiseFallDist(PI_WPC_SAVE) = MINRAISEFALLDIST_WPC_SAVE
	MinRaiseFallDist(PI_DOOR) = MINRAISEFALLDIST_DOOR
	MinRaiseFallDist(PI_TEACH) = MINRAISEFALLDIST_TEACH
	MinRaiseFallDist(PI_DUMMY_PICK_1) = MINRAISEFALLDIST_DUMMY_PICK_1
	MinRaiseFallDist(PI_DUMMY_PLACE_1) = MINRAISEFALLDIST_DUMMY_PLACE_1
	MinRaiseFallDist(PI_DUMMY_PICK_2) = MINRAISEFALLDIST_DUMMY_PICK_2
	MinRaiseFallDist(PI_DUMMY_PLACE_2) = MINRAISEFALLDIST_DUMMY_PLACE_2
	MinRaiseFallDist(PI_DUMMY_PICK_3) = MINRAISEFALLDIST_DUMMY_PICK_3
	MinRaiseFallDist(PI_DUMMY_PLACE_3) = MINRAISEFALLDIST_DUMMY_PLACE_3
	MinRaiseFallDist(PI_DUMMY_PICK_4) = MINRAISEFALLDIST_DUMMY_PICK_4
	MinRaiseFallDist(PI_DUMMY_PLACE_4) = MINRAISEFALLDIST_DUMMY_PLACE_4
	MinRaiseFallDist(PI_DUMMY_PICK_5) = MINRAISEFALLDIST_DUMMY_PICK_5
	MinRaiseFallDist(PI_DUMMY_PLACE_5) = MINRAISEFALLDIST_DUMMY_PLACE_5
	MinRaiseFallDist(PI_DUMMY_PICK_6) = MINRAISEFALLDIST_DUMMY_PICK_6
	MinRaiseFallDist(PI_DUMMY_PLACE_6) = MINRAISEFALLDIST_DUMMY_PLACE_6

	
	PosNames$(0) = "HOME"
	PosNames$(1) = "WPC_PLACE"
	PosNames$(2) = "RT_PICK"
	PosNames$(3) = "WPC_PICK"
	PosNames$(4) = "RT_PLACE"
	PosNames$(5) = "DROP_NOK"
	PosNames$(6) = "RT_SAVE"
	PosNames$(7) = "WPC_SAVE"
	PosNames$(8) = "DOOR"
	PosNames$(9) = "TEACH"
	PosNames$(10) = "DUMMY_PICK_1"
	PosNames$(11) = "DUMMY_PLACE_1"
	PosNames$(12) = "DUMMY_PICK_2"
	PosNames$(13) = "DUMMY_PLACE_2"
	PosNames$(14) = "DUMMY_PICK_3"
	PosNames$(15) = "DUMMY_PLACE_3"
	PosNames$(14) = "DUMMY_PICK_4"
	PosNames$(15) = "DUMMY_PLACE_4"
	PosNames$(14) = "DUMMY_PICK_5"
	PosNames$(15) = "DUMMY_PLACE_5"
	PosNames$(14) = "DUMMY_PICK_6"
	PosNames$(15) = "DUMMY_PLACE_6"

	'weight of gripper
	Weight 1.0

	' Define positioning accuracy for target points	
	Fine 500, 500, 500, 500
	
	'Init Outputs 	
	Off Sequence_Done
	Off HomingRequired
	
	PList
	SavePoints "robot1.pts"
	Print "PointList is stored"
	
Fend
Function fcSetSpeed(nSpeed As Integer, nAccel As Integer, nSpeedS As Integer, nAccelS As Integer)
	
		'Speed fast
		Speed nSpeed
		Accel nAccel, nAccel
		SpeedS nSpeedS
    	AccelS nAccelS, nAccelS
		
		Print " Speed % : ", nSpeed
		Print " Acceleration % : ", nAccel
		Print " Speed [mm/s] : ", nSpeedS
		Print " Acceleration [mm/s] : ", nAccelS
Fend

Function fcPowerSpeed()

	'Variables
	Integer nSpeed_fast, nSpeed_slow
	Integer nAccel_fast, nAccel_slow
	
	Integer nSpeedS_fast, nSpeedS_slow
	Integer nAccelS_fast, nAccelS_slow

	Integer nSpeedS_Max


  	'Speed for Job
	nSpeed_fast = In(Override)					'[%]
	nAccel_fast = 100							'[%]
					
	'SpeedS
	nSpeedS_Max = 2000 					'[mm/s]

	nSpeedS_fast = In(Override) * (nSpeedS_Max / 100)
	nAccelS_fast = 8000 						'[mm/s�]
	

	'Speed for Homing	
	nSpeed_slow = 5								'[%]
	nAccel_slow = 1								'[%]
	
	'5% of 2000mm/s	
	nSpeedS_slow = 100							'[mm/s]
	
	'1% of 8000mm/s�	
	nAccelS_slow = 80 	 						'[mm/s�]	




  	'Select Powermodus / Speed
	If Sw(E_Power_high) = 1 And Power = 0 Then

		'Power high
		Power High
		Print "Power high"
				
		'Speed fast
		fcSetSpeed(nSpeed_fast, nAccel_fast, nSpeedS_fast, nAccelS_fast)
	
	ElseIf Sw(E_Power_high) = 0 And Power = 1 Then
		'Power low
		Power Low
		Print "Power low"
		
		'Speed low
		fcSetSpeed(nSpeed_slow, nAccel_slow, nSpeedS_slow, nAccelS_slow)
	EndIf


	If Sw(E_Power_high) = 1 And (Speed <> In(Override)) Then
		'Speed fast	
		fcSetSpeed(nSpeed_fast, nAccel_fast, nSpeedS_fast, nAccelS_fast)

	ElseIf Sw(E_Power_high) = 0 And (Speed <> nSpeed_slow) Then
		'Speed slow	
		fcSetSpeed(nSpeed_slow, nAccel_slow, nSpeedS_slow, nAccelS_slow)

	EndIf

Fend
Function fcInitPrg(Prg_Nr As Integer, Prg_Name$ As String)

	Print "Start movement: Jump to point " + Prg_Name$
	
	Out (OUT_Prog_Nr), Prg_Nr
	Print "I/O: Set output 'OUT_Prg_Nr': " + Str$(Prg_Nr)
	Off Sequence_Done
	Print "I/O: Set output 'Sequence_Done': LOW"
	Off HomePosition
	Print "I/O: Set output 'HomePosition': LOW"
	
Fend


Function fcEndPrg
	Print "Movement finished"
	Out (OUT_Prog_Nr), 0
	Print "I/O: Set output 'OUT_Prg_Nr': 0"
	On Sequence_Done
	Print "I/O: Set output 'Sequence_Done': HIGH"

	Print "I/O: Wait for input 'Start_Prog': LOW"
	Wait Not Sw(Start_Prog)
	Print "I/O: input 'Start_prog'= HIGH"
	Off Sequence_Done
	Print "I/O: Set output 'Sequence_Done': LOW"
	Print "End fct end prg"
Fend


 Function fcPause

    Print "Pause"
    
	Wait 2

	Long X_Pos, Y_Pos, Z_Pos
    
	Print "Pause - Position stored"

	X_Pos = CX(RealPos)
	Y_Pos = CY(RealPos)
	Z_Pos = CZ(RealPos)
	
	Do While PauseOn
		If Not fcInPos(5, X_Pos, Y_Pos, Z_Pos) Then
			Print " Quit all "
			Quit All
		EndIf;
	Loop
Fend



Function fcCurrentPointID As Integer
	
	Integer i, pID
	
	pID = -1
		
	For i = 0 To 21
		If fcInPos(5, CX(P(i)), CY(P(i)), CZ(P(i))) Then
			pID = i
			Exit For
		EndIf
	Next

	fcCurrentPointID = pID
	
Fend



Function fcInPos(RangePos As Long, X_Point As Long, Y_Point As Long, Z_Point As Long) As Boolean

	'Check if the real position of the robot is near (RangePos in mm) the point	
    'fcInPos = (X_Point < (CX(RealPos) + RangePos)) And (X_Point > (CX(RealPos) - RangePos)) And (Y_Point < (CY(RealPos) + RangePos)) And (Y_Point > (CY(RealPos) - RangePos)) And (Z_Point < (CZ(RealPos) + RangePos)) And (Z_Point > (CZ(RealPos) - RangePos))

	fcInPos = (Abs(CX(RealPos) - X_Point) < RangePos) And (Abs(CY(RealPos) - Y_Point) < RangePos) And (Abs(CZ(RealPos) - Z_Point) < RangePos)


Fend


	

