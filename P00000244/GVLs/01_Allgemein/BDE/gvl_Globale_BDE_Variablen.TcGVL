﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6">
  <GVL Name="gvl_Globale_BDE_Variablen" Id="{22a6a841-6821-48f9-8983-c1020daeb928}">
    <Declaration><![CDATA[{attribute 'Tc2GvlVarNames'}
{attribute 'pack_mode' := '0'}


(*--------------------------------------------------------------------------------------
Preh IMA AUTOMATION AMBERG GMBH        

<DESC>
</DESC>

<KEYWORDS>
</KEYWORDS>

<DEPENDENCIES>
gvl_Anlagenkonfiguration(x.x.x)
</DEPENDENCIES>

<CHANGES>
Änderungen:					

Datum 		  |	Version	|	Autor		  |	Beschreibung		
-------------------------------------------------------------------------------------------------------------------------------------------------
2000-01-01	|	1.0.0		|	IMA	    	|	Start Programmierung
2010-11-12	|	2.0.0		|	MHU		  	|	LoginLevel => BDE_iLoginLevel
2011-11-07	|	2.1.0		|	MHU		 	|	Doppelklick auf Fehlertext oder Registerkarte: PLC-Variable BDE_iDblClickOnStatID wird beschrieben
2012-04-05	|	2.2.0		|	MHU		 	|	Neue Variable: BDE_sSchichtfuehrer
2013-05-13	|	3.0.0		|	MHU		  	|	BDE_GetErrorMessage => BDE_sGetErrorMessage, BDE_PutErrorNumber => BDE_iPutErrorNumber, BDE_Laeuft => BDE_bLaeuft,
                                    			Neue Variablen: BDE_sLoginUsername, BDE_sKommissionsNr / BDE_sSchichtfuehrer: STRING[20] => STRING[255]
2014-03-20	|	3.1.0		|	MHU		  	|	BDE_bStillstandsgrundErfasst
2017-04-12	|	3.2.0		|	MHU		  	|	Textanzeige-Meldungen im Klartext (mit ersetzten Token) ins PLC schreiben => BDE_asTextanzeige
2017-10-10	|	3.3.0		|	MHU		 	|	Loginvorgang durch PLC in BDE auslösen: BDE_sPerformLoginUsernameAndPassword
2017-11-14	|	3.3.1		|	MBE	    	|	Bausteinabhängigkeiten neu hinzu
			
</CHANGES>

<VERSION>
3.3.1
</VERSION>

--------------------------------------------------------------------------------------*)



(******************************************************************************************)
(* Alle hier deklarierten Variablen dienen als globales Interface zum Programm BDE-Online *)
(******************************************************************************************)

VAR_GLOBAL PERSISTENT (* BDE => PLC *)
	BDE_bLaeuft: 					BOOL := TRUE;	(* Wird von der BDE beschrieben, falls in Tabelle ProdDef (PD_TYPE=16) aktiviert *)
	BDE_bStillstandsgrundErfasst:	BOOL := TRUE;	(* Wird von der BDE beschrieben, abhängig davon ob im OEE-Modul ein Stillstandsgrund ausgewählt wurde *)
	BDE_bSchichtAngemeldet: BOOL;			(* Wird von der BDE beschrieben *)
	BDE_sSchichtfuehrer:	STRING[255];	(* Wird von der BDE beschrieben *)
	BDE_iLoginLevel:		INT := 0; 		(* 0=Nicht eingeloggt, 1..98=Konfigurierte Userlevel, 99=Administrator *)
	BDE_sLoginUsername:		STRING[255];	(* Wird von der BDE beschrieben *)
	BDE_iActiveLanguage:	INT := 1;		(* Von BDE: Aktuell angezeigte Sprache (1=Deutsch, 2=Englisch, 3=Schwedisch, 4=Französisch, 5=Bayrisch, 6=Ungarisch, 7=Tschechisch, 8=Spanisch, 9=Italienisch) *)
	BDE_iDblClickOnStatID:	INT := 0;		(* Wird beim Doppelklick auf eine Registerkarte in der Typverwaltung oder einem damit verknüpften Fehlertext auf die Nummer der Registerkarte gesetzt *)
END_VAR

VAR_GLOBAL PERSISTENT (* PLC => BDE *)
	BDE_bBdeSichtbar:	BOOL := TRUE;		(* Ein-/Ausblenden des BDE-Hauptfensters *)
	BDE_sKommissionsNr:	STRING[255];		(* Info aus PLC zur Identifikation des eingespielten SPS-Programms durch die BDE *)
	BDE_sPerformLoginUsernameAndPassword:	STRING[255];	(* PLC schreibt: "username|password" => BDE setzt auf "" und versucht einen Login; falsche Angaben => Logout *)
END_VAR

VAR_GLOBAL PERSISTENT (* PLC => BDE => PLC *)
	BDE_iPrintOrder:		INT := 0;		(* An BDE: Auftragsnummer, die gedruckt werden soll; Die BDE setzt anschließend die Variable wieder auf 0 zurück *)
END_VAR

VAR_GLOBAL PERSISTENT (* Fehlertext erfragen (z. B. für Hintergrundbild) *)
	BDE_iPutErrorNumber:	INT := -1;		(* PLC => BDE (Anfrage) *)
	BDE_sGetErrorMessage: 	STRING[255];	(* BDE => PLC (Antwort) *)
	BDE_asTextanzeige: ARRAY[1..MaxDisplay] OF STRING[255];	(* BDE => PLC (§Sprachtoken§, ~PLC-Token~ und *** sind ersetzt) *)
END_VAR

(***********************************************************************************************************
 * Warnung:
 * Alle Variablen, die von der BDE gelesen (=an die BDE gesendet) werden, dürfen nicht zyklisch,
 * sondern nur für einen Zyklus beschrieben werden, selbst wenn es sich immer um den gleichen Wert handelt! 
 ***********************************************************************************************************)

(* Im Main einfügen:
BDE_sKommissionsNr := SystemInfo.projectName; (* Projektkennung an BDE senden *)
 *)
]]></Declaration>
  </GVL>
</TcPlcObject>