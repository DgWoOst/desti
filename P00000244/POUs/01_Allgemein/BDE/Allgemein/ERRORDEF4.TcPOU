﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6">
  <POU Name="ERRORDEF4" Id="{c0e43f40-9f42-4de0-bfa3-3d4956b51ee6}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK ERRORDEF4

(*--------------------------------------------------------------------------------------
Preh IMA AUTOMATION AMBERG GMBH        

<DESC>
Ersatz für den Baustein ERRORDEF, falls in einer Schrittkette mehrere
Fehlerarray mit der gleichen FaultID verwendet werden sollen
1. Fehlerarray => ERRORDEF
2. Fehlerarray => ERRORDEF2
3. Fehlerarray => ERRORDEF3
4. Fehlerarray => ERRORDEF4
</DESC>

<KEYWORDS>

</KEYWORDS>

<CHANGES>
Änderungen:					

Datum 		|	Version		|	Autor		|	Beschreibung		
-------------------------------------------------------------------------------------------------------------------------------------------------
2014-02-18	|	1.0.0		|	IMA	    	|	Ursprungsversion
2017-05-17	|	2.0.0		|	SBA		|	Quittieren nur wenn Fehler auch gesetzt (F[I] = TRUE) hinzu (falls Signatur doppelt kann Fehler jetzt auch quittiert werden)
			|				|		    	|	
			
</CHANGES>

<VERSION>
2.0.0
</VERSION>

--------------------------------------------------------------------------------------*)

VAR
	I: INT; (* Index *)
END_VAR
VAR_INPUT
	ID: BYTE; (* eindeutige Nummer der Schrittkette *)
	FAnz: INT; (* akt. Fehleranzeige *)
	StNr: STRING(255);
	EF: ARRAY [1..ErrMaxBits] OF ERRDAT; (* Fehlerdefinitionen *)
END_VAR
VAR_IN_OUT
	F: ARRAY [1..ErrMaxBits] OF BOOL; (* Fehlerfeld *)
END_VAR
VAR PERSISTENT
	BDEFehler: ARRAY [1 .. ErrMaxBits] OF BOOL :=[ ErrMaxBits(FALSE)];
END_VAR
VAR
	Tmp: BOOL;
END_VAR















]]></Declaration>
    <Implementation>
      <ST><![CDATA[(* BDE Fehler kommt/geht eintragen *)
(* Änderung 04.11.99 mwi: Frühwarnungen werden nicht versendet *)
FOR I := 1 TO ErrMaxBits BY 1 DO
 	IF (F[I] XOR BDEFehler[I]) AND (EF[i].Prio <> ErrFW) AND (EF[i].Prio <> ErrPreFw) THEN (* Fehler kommt/geht *)
		IF BDEAddErrTel(F[I], ID, 48 + DINT_TO_BYTE(I)) THEN
			BDEFehler[I] := F[I];
		END_IF;
	END_IF;
	IF F[I] AND BDEResend AND (EF[i].Prio <> ErrFW)AND (EF[i].Prio <> ErrPreFw) THEN (* Fehler - Resend *)
		Tmp := BDEAddErrTel(F[I], ID, 48 + DINT_TO_BYTE(I));
	END_IF
END_FOR;

(* akt. Stationsfehler eintragen *)
FOR I := 1 TO ErrMaxBits BY 1 DO
	IF F[I] THEN
		IF EF[I].Prio < ErrorDisplay[FAnz].ActError.Prio THEN
			(* Fehler eintragen *)
			ErrorDisplay[FAnz].ActError.ID := ID; (* Stations - ID mit übergeben; neu seit 24.07.2003, mwi *)
			ErrorDisplay[FAnz].ActError.Prio := EF[I].Prio;
			ErrorDisplay[FAnz].ActError.Nr := EF[I].Nr;
			ErrorDisplay[FAnz].ActError.Txt := LEFT(CONCAT(CONCAT(StNr, EF[I].Txt),'               '),15);
		END_IF;
	END_IF
END_FOR;

(* Alle Fehler löschen *)
IF ErrorDisplay[FAnz].DelError.Prio = ErrResetAll THEN
	FOR I := 1 TO ErrMaxBits BY 1 DO
		F[I] := FALSE;
	END_FOR;
END_IF;

(* Fehler löschen *)
IF ErrorDisplay[FAnz].DelError.Nr <> 0 THEN
	FOR I := 1 TO ErrMaxBits BY 1 DO
		IF 		(ErrorDisplay[FAnz].DelError.Nr = EF[I].Nr) 
			AND (ErrorDisplay[FAnz].DelError.Txt = LEFT(CONCAT(CONCAT(StNr, EF[I].Txt),'               '),15))
			AND	F[I] (* 2017-05-17; SBA; Quittieren nur wenn Fehler auch gesetzt hinzu (falls Signatur doppelt kann Fehler jetzt auch quittiert werden) *)	 
		THEN
			(* Fehlerbit löschen *)
			F[I] := FALSE;
			EXIT;
		END_IF;
	END_FOR;
END_IF;
]]></ST>
    </Implementation>
    <LineIds Name="ERRORDEF4">
      <LineId Id="3" Count="35" />
      <LineId Id="105" Count="3" />
      <LineId Id="42" Count="5" />
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>