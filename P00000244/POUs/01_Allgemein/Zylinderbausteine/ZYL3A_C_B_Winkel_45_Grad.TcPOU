﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6">
  <POU Name="ZYL3A_C_B_Winkel_45_Grad" Id="{67fafe5c-9c6b-4d95-88b6-98d1aa39f988}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK ZYL3A_C_B_Winkel_45_Grad

(*
Preh IMA AUTOMATION AMBERG GMBH        

<DESC>
!!!! Sonderbaustein wegen C-Lage in 45° Deshalb müssen Zwischenposzylinder anderts fahren um die C-Lage zu erreichen ++WST++!!!!

Dieser Baustein steuert eine Dreheinheit mit drei Abfragen winkelabhängig an.
Die Dreheinheit hat zwei Zylinder um den Kolben von der A-Lage bzw. von der B-Lage in die 
C-Lage zu drücken.
Die mittlere Lage ist die C-Lage.
Sind beide Zylinder nicht angsteuert fährt der Zylinder nach der A-Lage.
Sind beide Zylinder angsteuert fährt der Zylinder nach der B-Lage.
Ist einer angesteuert und der andere nicht so wird die C-Lage angefahren (Mittelstellung).
Die Fehler werden lokal erzeugt.
Als StationsID kann die gleiche ID wie die Station hat übertragen werden.

Ursprung:
=========
ZYL3A_C_B

Festlegung A = 0 Grad
Festlegung C = 45 Grad
Festlegung B = 180 Grad
</DESC>

<KEYWORDS>

</KEYWORDS>

<CHANGES>
Änderungen:					

Datum 		|	Version	|	Autor	|	Beschreibung		
-------------------------------------------------------------------------------------------------------------------------------------------------
2014-11-03	|	1.00	|	GST		|	Ursprungsversion	
2015-02-05	|	2.20	|	GST		|	Stringlänge reduziert bei txtVentil
2015-04-16	|	2.22	|	WST		|	Baustein dreht um 180° zum Einsetzenm Abholen ist auf 45°. 
										Auf den jeweiligen Zwischenanschlag kann dan nur noch von einer Seite gefahren werden.
										(Mechanisch bedingt) Deshal muss bei Drehen von A nach 45° mit dem Zwischenzylinder das
										Drehmodul überdrückt werden.
2015-04-17	|	2.30	|	MHU		|	Neuer Input-Parameter 'Bild' zur Auswahl der Zylinderbilder für die Schrittkettendiagnose
			|			|			|	Integration von g_bOnlineChange
2015-08-16	|	2.31	|	PAM		|	ZwischenNr integriert
2016-05-10	|	3.00	|	GST		|	Integration Condition Monitoring
2016-05-10	|	3.10	|	GST		|	Berechnung der Schaltzeiten auch beim manuellen Betrieb			
</CHANGES>

<VERSION>
3.10
</VERSION>

*)

VAR_INPUT PERSISTENT
    grundbed: 	BOOL;				(* Zentrale *)
    akt_winkel: INT;				(* Aktueller Winkelwert *)
	a: 			BOOL; 				(* Näherungsschalter Ruhe *)
	b: 			BOOL; 				(* Näherungsschalter Arbeit *)
	c:			BOOL;				(* Näherungsschalter Zwischenposition *)
    winkel_ab:	INT; 				(* Winkelwert für Fahren nach "A" oder "B" *)
    winkel_c:	INT; 				(* Winkelwert Zylinder Signal "C" *)
    bed_b: 		BOOL;				(* FALSE = Fahren auf "A", TRUE = Fahren auf "B" bei Winkel_ab *)
    kon_ab: 	INT; 				(* Winkelwert für Kontrolle Signal "A" oder "B" *)
    kon_c:	 	INT; 				(* Winkelwert für Kontrolle Signal "C" *)
	enable_man:	BOOL := TRUE;		(* Freigabe der manuellen Zylinderbewegung *)
	FNrDrehen:	INT;				(* Fehlernummer der Dreheinheit *)
	FNrManDrehen:		INT;		(* Fehlernummer für Zylinder manuell angesteuert *)
	FNrManZwisch:		INT;		(* Fehlernummer für Zylinder manuell angesteuert *)
	FAnz:		INT;				(* Textanzeige *)
	StNr:		STRING(15);			(* Stationsstring *)
	ZylNr:		STRING(15);			(* Zylinderstring *)
	ZwischenNr:	STRING(15);			(* Zwischenhubstring *)
	txtVentil:	STRING(15) := g_txtVentil;
									(* Zylinderbezeichnung -
									alte Bezeichnung
											'Y',
									neue Bezeichnung etwa 
											'KH' für Ventil, Wegeventil, Pneumatikventil
											'FL' für Sicherheitsventil
											'GS' für Vakuuminjektor
											'RN' für Druckregelventil
											'RM' für Drosselrückschlagventil
											'SJ' für Druckknopfventil 
									Bezeichnung Zylinder bei Optionaler Beschriftung 'A 128.0'
									*)
	txtZwischenhub:	STRING(15) := g_txtVentil;
									(* Zylinderbezeichnung für den Zwischenhub -
									alte Bezeichnung
											'Y',
									neue Bezeichnung etwa 
											'KH' für Ventil, Wegeventil, Pneumatikventil
											'FL' für Sicherheitsventil
											'GS' für Vakuumejektor
											'RN' für Druckregelventil
											'RM' für Drosselrückschlagventil
											'SJ' für Druckknopfventil 
									Bezeichnung Zylinder bei Optionaler Beschriftung 'A 128.0'
									*)
	txtA:		STRING(15);			(* Bezeichnung Sensor A nur bei Optionaler Beschriftung 'E 128.0' *)
	txtB:		STRING(15);			(* Bezeichnung Sensor B nur bei Optionaler Beschriftung 'E 128.1' *)
	txtC:		STRING(15);			(* Bezeichnung Sensor C nur bei Optionaler Beschriftung 'E 128.2' *)
	ID: 		BYTE; 				(* eindeutige Nummer der Schrittkette *)
	Bild:		TZylPicture := picStandard;	(* Anzuzeigendes Zylinderbild in der Schrittkettendiagnose für Stellung A *)
	BildB:		TZylPicture := picStandard;	(* Anzuzeigendes Zylinderbild in der Schrittkettendiagnose für Stellung B *)
	BildC:		TZylPicture := picStandard;	(* Anzuzeigendes Zylinderbild in der Schrittkettendiagnose für Stellung C *)
END_VAR

VAR_OUTPUT PERSISTENT
	drehen:		BOOL;				(* Zustand der Dreheinheit *)
	zwisch:		BOOL;				(* Zustand des Zwischenhubs *)
	err:		BOOL;				(* Zylinderfehler *)
	man_drehen:	BOOL;				(* Manuelle Ansteuerung der Dreheinheit *)
	man_zwisch:	BOOL;				(* Manuelle Ansteuerung des Zwischenhubs *)
    i_ab: 		BOOL; 				(* Impuls Zylinder "A" oder "B" *)
    i_c: 		BOOL; 				(* Impuls Zylinder "C" *)
    frg:		BOOL;				(* Stationsfreigabe *)
END_VAR

VAR PERSISTENT
    fl_spur: 	BOOL; 					(* Flankenmerker Spur *)
    wert1: 		INT; 					(* Ein - 10 Grad *)
    wert2: 		INT; 					(* Aus - 10 Grad *)
    hs1: 		BOOL; 						(* Hilfsspur 1 *)
    hs2: 		BOOL; 						(* Hilfsspur 2 *)
    hs3: 		BOOL; 						(* Hilfsspur 3 *)
	TmpYDreh: 	BOOL;
	TmpYZwisch: BOOL;
END_VAR

VAR
	f1: 		ARRAY [1..ErrMaxBits] OF BOOL; 	(* Fehlerbits *)
	Errors1: 	ARRAY[1..ErrMaxBits] OF ERRDAT;	(* Fehlernummer und Text *)
	Fehler1:	ERRORZYL;						(* Instanz zur Fehleranzeige *)
	tmp: 		STRING;							(* Temporärer String *)
	pos:		BOOL;
END_VAR

VAR PERSISTENT
	hm_drehen:	BOOL;							(* Flankenerkennung Signal "drehen" *)
	hm_zwisch:	BOOL;							(* Flankenerkennung Signal "zwisch" *)
	diTimeToA:	DINT;							(* Verfahrzeit -> A *)
	diTimeToB:	DINT;							(* Verfahrzeit -> B *)
	diTimeToC:	DINT;							(* Verfahrzeit -> C *)
	bRunning:	BOOL;							(* Zylinder verfährt *)
END_VAR



]]></Declaration>
    <Implementation>
      <ST><![CDATA[(* Winkelwert für Hilfsspur bilden *)
wert1 := (winkel_ab + 350) MOD 360;
wert2 := (winkel_c + 350) MOD 360;

hs1:=NOCKE(akt_winkel:=akt_winkel,ein:=winkel_ab,aus:=winkel_c);
hs2:=NOCKE(akt_winkel:=akt_winkel,ein:=wert1,aus:=wert2);
hs3:=NOCKE(akt_winkel:=akt_winkel,ein:=kon_ab,aus:=kon_c);

(* Ansteuerung Zylinder *)
i_ab := FALSE;
i_c := FALSE;

IF hs1 AND hs2 AND NOT fl_spur THEN
	fl_spur := TRUE;
	i_ab := TRUE;
END_IF;

IF NOT hs1 AND NOT hs2 AND fl_spur THEN
	fl_spur := FALSE;
	i_c := TRUE;
END_IF;

(* Drehmodul *)

(* Position A ansteuern *)
IF i_ab AND NOT bed_b THEN
	TmpYDreh := FALSE;
	TmpYZwisch := FALSE;
END_IF;

(* Position B ansteuern *)
IF i_ab AND bed_b THEN
	TmpYDreh := TRUE;
	TmpYZwisch := TRUE;
END_IF;

(* ++WST++ 2015-04-16
   Ansteuerung geändet wegen 45° Position der C-Lage. Kann mechanisch nicht mehr mit fahren
   auf die Zwischenanschläge gelöst werden, deshalb muss von A->C mit dem ZP überdrückt werden *)

(* Position C ansteuern von A kommend = 45° *)
IF (i_c AND NOT TmpYDreh AND NOT TmpYZwisch) OR urloesch THEN
	TmpYZwisch:= NOT TmpYDreh;
END_IF;

(* Position C ansteuern von B kommend = 135° *)
IF (i_c AND TmpYDreh AND TmpYZwisch) OR urloesch THEN
	TmpYDreh := NOT TmpYZwisch;
END_IF;

(* Eintrag der Dreheinheit in die Schrittkettendiagnose *)
IF Zyl_AktZylinder < Zyl_MaxZylinder THEN
	Zyl_AktZylinder := Zyl_AktZylinder + 1;

	(* Zylinderbild in Schrittkettendiagnose passend anzeigen *)
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Bild := INT_TO_BYTE(Bild);
	IF (BildB <> picStandard) AND Drehen AND Zwisch THEN
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Bild := INT_TO_BYTE(BildB + 1 - 2 * BOOL_TO_INT(BildB MOD 2 = 1));
	END_IF;
	IF (BildC <> picStandard) AND ((Drehen AND NOT Zwisch) OR (NOT Drehen AND Zwisch)) THEN
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Bild := INT_TO_BYTE(BildC + 1 - 2 * BOOL_TO_INT(BildC MOD 2 = 1));
	END_IF;

	IF Neustart OR g_bOnlineChange THEN
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Bild := INT_TO_BYTE(Bild);
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].D := FALSE;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].FNr := FNrManDrehen;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Typ := 3;
		IF StNr <> '' OR ZylNr <> '' THEN
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtA :=
				LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,g_txtA)),15);
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtB :=
				LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,g_txtB)),15);
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtC :=
				LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,g_txtC)),15);
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY :=
				LEFT(CONCAT(CONCAT(StNr,txtVentil),ZylNr),15);
		ELSE
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtA := txtA;
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtB := txtB;
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtC := txtC;
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY := txtVentil;
		END_IF;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtD := '';
	END_IF;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].A := a;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].B := b;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].C := c;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].on := drehen;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].man := man_drehen;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].enable_man := enable_man;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Fault := err;

	(* Zylinder manuell verfahren *)
	IF 	Zyl_Chains[Zyl_AktModul].aStatus[Zyl_AktChain].Manual THEN
		IF	Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_on AND enable_man THEN
		(* Zylinder manuell einschalten *)
			man_drehen := TRUE;
		END_IF;

		(* Zylinder manuell ausschalten *)
		IF 	Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_off AND enable_man THEN
			man_drehen := FALSE;
		END_IF;
	ELSE
		man_drehen := TmpYDreh;
	END_IF;
END_IF;

(* Ansteuerung von BDE-Online zurücksetzen *)
Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_on := FALSE;
Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_off := FALSE;

(* Keba Panel *)
KebaInitZyl(FNrMan := FNrManDrehen, ZylTyp := 3, txtVentil :=
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY);

(* Zwischenanschlagmodul *)
IF Zyl_AktZylinder < Zyl_MaxZylinder THEN
	Zyl_AktZylinder := Zyl_AktZylinder + 1;
	IF Neustart OR g_bOnlineChange THEN
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].A := FALSE;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].B := FALSE;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].C := FALSE;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].D := FALSE;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].FNr := FNrManZwisch;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Typ := 0;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtA := '';
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtB := '';
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtC := '';
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtD := '';
		IF StNr <> '' OR ZylNr <> '' THEN
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY :=
				LEFT(CONCAT(CONCAT(StNr,txtZwischenhub),ZwischenNr),15);
		ELSE
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY := txtZwischenhub;
		END_IF;
	END_IF;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].on := zwisch;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].man := man_zwisch;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].enable_man := enable_man;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Fault := err;

	(* Zylinder manuell verfahren *)
	IF 	Zyl_Chains[Zyl_AktModul].aStatus[Zyl_AktChain].Manual THEN
		IF	Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_on AND enable_man THEN
		(* Zylinder manuell einschalten *)
			man_zwisch := TRUE;
		END_IF;

		(* Zylinder manuell ausschalten *)
		IF 	Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_off AND enable_man THEN
			man_zwisch := FALSE;
		END_IF;
	ELSE
		man_zwisch := TmpYZwisch;
	END_IF;

	(* Ausgangszuweisung *)
	Drehen := man_drehen;
	Zwisch := man_zwisch;
	
	IF (hm_drehen <> Drehen) OR (hm_zwisch <> Zwisch) THEN
		(* Ermittlung der Hübe *)
		Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diStrokes :=
			Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diStrokes + 1;

		(* Zurücksetzen der Verfahrzeiten *)
		bRunning := TRUE;

		IF NOT TmpYDreh AND NOT TmpYZwisch THEN
			diTimeToA := 0;
		ELSIF TmpYDreh AND TmpYZwisch THEN
			diTimeToB := 0;
		ELSE
			diTimeToC := 0;
		END_IF;
	END_IF;
END_IF;

(* Zylinder meldet pos wenn manuelle Ansteuerung übereinstimmt *)
(* hier nur für die Berechnung der Zylinderzeit *)
Pos := ((	 a AND NOT b AND NOT c AND NOT TmpYDreh AND NOT TmpYZwisch) OR
            (NOT a AND b AND NOT c AND TmpYDreh AND TmpYZwisch) OR
            (NOT a AND NOT b AND c AND ((NOT TmpYDreh AND TmpYZwisch) OR
                                        (TmpYDreh AND NOT TmpYZwisch))));

(* Abspeichern der Verfahrzeiten *)
IF bRunning THEN
	IF pos THEN
		IF NOT TmpYDreh AND NOT TmpYZwisch THEN
			Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diTimeToA := diTimeToA;
		ELSIF TmpYDreh AND TmpYZwisch THEN
			Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diTimeToB := diTimeToB;
		ELSE
			Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diTimeToC := diTimeToC;
		END_IF;
		bRunning := FALSE;
	ELSE
		IF NOT TmpYDreh AND NOT TmpYZwisch THEN
			diTimeToA := diTimeToA + g_diTaskZyklus;
		ELSIF TmpYDreh AND TmpYZwisch THEN
			diTimeToB := diTimeToB + g_diTaskZyklus;
		ELSE
			diTimeToC := diTimeToC + g_diTaskZyklus;
		END_IF;
	END_IF;
END_IF;

hm_drehen := Drehen;
hm_zwisch := Zwisch;

(* Fehlerfeld initialisieren bei Neustart *)
IF Neustart OR g_bOnlineChange THEN
	IF StNr <> '' OR ZylNr <> '' THEN
		Errors1[1].Txt := LEFT(CONCAT(CONCAT(StNr,txtVentil),CONCAT(ZylNr,'               ')),15);

		Errors1[2].Txt := LEFT(CONCAT(CONCAT(StNr,txtZwischenhub),CONCAT(ZwischenNr,'               ')),15);

		tmp := F_ToLCase(g_txtA);
		tmp := CONCAT( tmp, '               ');
		Errors1[3].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		tmp := F_ToLCase(g_txtB);
		tmp := CONCAT( tmp, '               ');
		Errors1[4].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		tmp := F_ToLCase(g_txtC);
		tmp := CONCAT( tmp, '               ');
		Errors1[5].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		tmp := F_ToUCase(g_txtA);
		tmp := CONCAT( tmp, '               ');
		Errors1[6].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		tmp := F_ToUCase(g_txtB);
		tmp := CONCAT( tmp, '               ');
		Errors1[7].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		tmp := F_ToUCase(g_txtC);
		tmp := CONCAT( tmp, '               ');
		Errors1[8].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		Errors1[6].Nr := FNrDrehen;
		Errors1[6].Prio := ErrZyl;
		Errors1[6].ID := ID;

		Errors1[7].Nr := FNrDrehen;
		Errors1[7].Prio := ErrZyl;
		Errors1[7].ID := ID;

		Errors1[8].Nr := FNrDrehen;
		Errors1[8].Prio := ErrZyl;
		Errors1[8].ID := ID;
	ELSE
		Errors1[1].Txt := LEFT(CONCAT(txtVentil,'               '),15);
		Errors1[2].Txt := LEFT(CONCAT(txtZwischenhub,'               '),15);
		Errors1[3].Txt := LEFT(CONCAT(txtA,'               '),15);
		Errors1[4].Txt := LEFT(CONCAT(txtB,'               '),15);
		Errors1[5].Txt := LEFT(CONCAT(txtC,'               '),15);
	END_IF;

	Errors1[1].Nr := FNrManDrehen;
	Errors1[1].Prio := ErrZylMan;
	Errors1[1].ID := ID;

	Errors1[2].Nr := FNrManZwisch;
	Errors1[2].Prio := ErrZyl;
	Errors1[2].ID := ID;

	Errors1[3].Nr := FNrDrehen;
	Errors1[3].Prio := ErrZyl;
	Errors1[3].ID := ID;

	Errors1[4].Nr := FNrDrehen;
	Errors1[4].Prio := ErrZyl;
	Errors1[4].ID := ID;

	Errors1[5].Nr := FNrDrehen;
	Errors1[5].Prio := ErrZyl;
	Errors1[5].ID := ID;

END_IF;

(*    Zylinderfehler    *)
(*----------------------*)
IF grundbed THEN
	(* Dreheinheit manuell angesteuert *)
	f1[1] := (drehen <> man_drehen);

	(* Zwischenhub manuell angesteuert *)
	f1[2] := (zwisch <> man_zwisch);

	IF NOT	Zyl_Chains[Zyl_AktModul].aStatus[Zyl_AktChain].Manual THEN
		(* Fehler A nicht "0" *)
		f1[3] := f1[3] OR	(a AND c AND (TmpYDreh OR TmpYZwisch)) OR
						    (a AND b AND TmpYDreh);
		(* Fehler B nicht "0" *)
		f1[4] := f1[4] OR (b AND c AND ( NOT TmpYDreh OR NOT TmpYZwisch)) OR
	                        (a AND b AND NOT TmpYDreh);
		(* Fehler C nicht "0" *)
		f1[5] := f1[5] OR (a AND c AND NOT TmpYDreh AND NOT TmpYZwisch) OR
	                        (b AND c AND TmpYDreh AND TmpYZwisch);
		IF StNr <> '' OR ZylNr <> '' THEN
			(* Fehler A nicht "1" *)
			f1[6] := f1[6] OR (NOT a AND NOT TmpYDreh AND NOT TmpYZwisch AND hs1 AND hs2 AND hs3);
			(* Fehler B nicht "1" *)
			f1[7] := f1[7] OR (NOT b AND TmpYDreh AND TmpYZwisch AND hs1 AND hs2 AND hs3);
			(* Fehler C nicht "1" *)
			f1[8] := f1[8] OR (NOT c AND ((NOT TmpYDreh AND TmpYZwisch) OR (TmpYDreh AND NOT TmpYZwisch)) AND NOT hs1 AND NOT hs2 AND NOT hs3);
		ELSE
			(* Fehler A nicht "1" *)
			f1[3] := f1[3] OR (NOT a AND NOT TmpYDreh AND NOT TmpYZwisch AND hs1 AND hs2 AND hs3);
			(* Fehler B nicht "1" *)
			f1[4] := f1[4] OR (NOT b AND TmpYDreh AND TmpYZwisch AND hs1 AND hs2 AND hs3);
			(* Fehler C nicht "1" *)
			f1[5] := f1[5] OR (NOT c AND ((NOT TmpYDreh AND TmpYZwisch) OR (TmpYDreh AND NOT TmpYZwisch)) AND NOT hs1 AND NOT hs2 AND NOT hs3);
		END_IF;
	END_IF;
END_IF;

(* Freigabe *)
frg := TRUE;

(* Zylinder erreicht A-Lage nicht *)
(* oder hat noch C-Lage *)
IF (c OR NOT a) AND NOT drehen AND hs1 AND hs2 AND hs3 THEN
	frg := FALSE;
END_IF;

(* Zylinder erreicht B-Lage nicht *)
(* oder hat noch C-Lage *)
IF (c OR NOT b) AND drehen AND hs1 AND hs2 AND hs3 THEN
	frg := FALSE;
END_IF;

(* Zylinder erreicht C-Lage nicht *)
(* oder hat noch A-Lage oder B-Lage *)
IF (a OR b OR NOT c) AND NOT hs1 AND NOT hs2 AND NOT hs3 THEN
	frg := FALSE;
END_IF;

IF StNr <> '' OR ZylNr <> '' THEN
	(* Auswertung des Fehlerbausteins *)
	Fehler1(ID:=ID,FAnz:=FAnz,EF:=Errors1,F:=f1,Anzahl:=8);

	err := f1[1] OR f1[2] OR f1[3] OR f1[4]
			OR f1[5] OR f1[6] OR f1[7] OR f1[8];
ELSE
	(* Auswertung des Fehlerbausteins *)
	Fehler1(ID:=ID,FAnz:=FAnz,EF:=Errors1,F:=f1,Anzahl:=5);

	err := f1[1] OR f1[2] OR f1[3] OR f1[4]
			OR f1[5];
END_IF;

(* Ansteuerung von BDE-Online zurücksetzen *)
Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_on := FALSE;
Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_off := FALSE;

(* Keba Panel *)
KebaInitZyl(FNrMan := FNrManZwisch, ZylTyp := 0, txtVentil :=
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY);]]></ST>
    </Implementation>
    <LineIds Name="ZYL3A_C_B_Winkel_45_Grad">
      <LineId Id="3" Count="361" />
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>