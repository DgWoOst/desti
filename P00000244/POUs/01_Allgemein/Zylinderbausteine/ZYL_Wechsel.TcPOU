﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6">
  <POU Name="ZYL_Wechsel" Id="{ed323365-6a66-4253-9c53-3326dc2cf9c5}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK ZYL_Wechsel
(*
Preh IMA AUTOMATION AMBERG GMBH        

<DESC>
Winkelabhängige Zylinderansteuerung mit Abfragen (abgesichert)
Bei Winkelwert "wech" übernimmt der Zylinder den Zustand bed_wech
</DESC>

<KEYWORDS>

</KEYWORDS>

<CHANGES>
Änderungen:

Datum 		|	Version	|	Autor	|	Beschreibung		
-------------------------------------------------------------------------------------------------------------------------------------------------
2013-03-25	|	2.10	|	GST		|	Ursprungsversion
2013-04-05	|	2.11	|	GST		|	Stringlänge auf 15 Zeichen mit Left(xxx,15) reduziert
2013-10-09	|	2.12	|	GST		|	Bei Uebergabe des kompletten Fehlertext werden 3 statt 5 Fehler erzeugt
2013-11-13	|	2.13	|	GST		|	Bei der Abfrage bei Zylinder mit einen Sensor (AND NOT hs1)
2013-12-20	|	2.14	|	GST		|	Manuelle Ansteuerung geändert von y := on; auf y := man;
2015-02-05	|	2.20	|	GST		|	Stringlänge reduziert bei txtVentil
2015-04-17	|	2.30	|	MHU		|	Neuer Input-Parameter 'Bild' zur Auswahl der Zylinderbilder für die Schrittkettendiagnose
			|			|			|	Integration von g_bOnlineChange
2015-02-05	|	2.31	|	GST		|	Überschneidung A und B-Lage bei Fehlerauswertung zugelassen
2016-05-10	|	3.00	|	GST		|	Integration Condition Monitoring
2016-05-10	|	3.10	|	GST		|	Berechnung der Schaltzeiten auch beim manuellen Betrieb
|		    	|			
</CHANGES>

<VERSION>
3.10
</VERSION>

*)



VAR_INPUT PERSISTENT
	iTyp:		SINT:= 2;			(* Zylindertyp 	0: ZYL0 Zylinder ohne Abfragen *)
									(* Zylindertyp  1: ZYL1A Zylinder nur mit Abfrage A *)
									(* Zylindertyp -1: ZYL1B Zylinder nur mit Abfrage B *)
									(* Zylindertyp  2: ZYL2 Zylinder mit Abfrage A + B *)
    grundbed: 	BOOL;				(* Zentrale *)
    akt_winkel: INT;				(* Aktueller Winkelwert *)
    wech: 		INT; 				(* Winkelwert Zylinder Signal wechseln *)
    bed_wech: 	BOOL;				(* Bedingung für Signal wechseln *)
    kon_wech: 	INT;				(* Winkelwert Kontrolle Signal wechseln *)
	a: 			BOOL; 				(* Näherungsschalter Ruhe *)
	b: 			BOOL; 				(* Näherungsschalter Arbeit *)
	enable_man:	BOOL := TRUE;		(* Freigabe der manuellen Zylinderbewegung *)
	FNr:		INT;				(* Fehlernummer *)
	FNrMan:		INT;				(* Fehlernummer für Zylinder manuell angesteuert *)
	FAnz:		INT;				(* Textanzeige *)
	StNr:		STRING(15);			(* Stationsstring *)
	ZylNr:		STRING(15);			(* Zylinderstring *)
	txtVentil:	STRING(15) := g_txtVentil;
									(* Zylinderbezeichnung -
									alte Bezeichnung
											'Y',
									neue Bezeichnung etwa 
											'KH' für Ventil, Wegeventil, Pneumatikventil
											'FL' für Sicherheitsventil
											'GS' für Vakuuminjektor
											'RN' für Druckregelventil
											'RM' für Drosselrückschlagventil
											'SJ' für Druckknopfventil 
									Bezeichnung Zylinder bei Optionaler Beschriftung 'A 128.0'
									*)
	txtA:		STRING(15);			(* Bezeichnung Sensor A nur bei Optionaler Beschriftung 'E 128.0' *)
	txtB:		STRING(15);			(* Bezeichnung Sensor B nur bei Optionaler Beschriftung 'E 128.1' *)
	ID: 		BYTE; 				(* eindeutige Nummer der Schrittkette *)
	Bild:		TZylPicture := picStandard;	(* Anzuzeigendes Zylinderbild in der Schrittkettendiagnose für Grundstellung *)
	BildB:		TZylPicture := picStandard;	(* Anzuzeigendes Zylinderbild in der Schrittkettendiagnose für Arbeitsstellung *)
END_VAR
VAR_OUTPUT PERSISTENT
	y:			BOOL;				(* Zustand Zylinder *)
	err:		BOOL;				(* Zylinderfehler *)
	man:		BOOL;				(* Manuelle Ansteuerung des Ventils *)
    i_wech:		BOOL;				(* Impuls Wechseln *)
    frg:		BOOL;				(* Stationsfreigabe *)
END_VAR
VAR PERSISTENT
	on:			BOOL;				(* Aktueller Zustand des Ventils *)
    fl_spur: 	BOOL;				(* Flankenmerker Spur *)
    wert1: 		INT;				(* Winkelwert nach "wech" vor "kon_wech" *)
    wert2: 		INT;				(* Winkelwert nach "kon_wech" vor "wech" *)
    hs1: 		BOOL;				(* Hilfsspur 1 *)
    hs2: 		BOOL;				(* Hilfsspur 2 *)
END_VAR

VAR
	f1: 		ARRAY [1..ErrMaxBits] OF BOOL; (* Fehlerfeld *)
	Errors1: 	ARRAY[1..ErrMaxBits] OF ERRDAT;
	Fehler1:	ERRORZYL;
	tmp: 		STRING;
	pos:		BOOL;
END_VAR

VAR PERSISTENT
	hm_y:		BOOL;							(* Flankenerkennung Signal "y" *)
	diTimeToA:	DINT;							(* Verfahrzeit -> A *)
	diTimeToB:	DINT;							(* Verfahrzeit -> B *)
	bRunning:	BOOL;							(* Zylinder verfährt *)
END_VAR





]]></Declaration>
    <Implementation>
      <ST><![CDATA[(* Winkelabhängige Zylinderansteuerung mit Abfragen *)
(* Je nach Bedingung wechselt der Zylinder nach a bzw. b *)

(* Winkelwert für Hilfsspur bilden *)
wert1 := (wech + 350) MOD 360;
wert2 := (kon_wech + 350) MOD 360;

hs1:=NOCKE(akt_winkel:=akt_winkel,ein:=wech,aus:=kon_wech);
hs2:=NOCKE(akt_winkel:=akt_winkel,ein:=wert1,aus:=wert2);

(* Ansteuerung Zylinder *)
i_wech := FALSE;

IF hs1 AND hs2 AND NOT fl_spur THEN
	fl_spur := TRUE;
	i_wech := TRUE;
END_IF;

IF NOT hs1 AND NOT hs2 AND fl_spur THEN
	fl_spur := FALSE;
END_IF;

IF i_wech AND bed_wech THEN
	on := TRUE;
END_IF;

IF i_wech AND NOT bed_wech THEN
	on := FALSE;
END_IF;

(* Initialisierung *)
IF Zyl_AktZylinder < Zyl_MaxZylinder THEN
	Zyl_AktZylinder := Zyl_AktZylinder + 1;

	(* Zylinderbild in Schrittkettendiagnose passend anzeigen *)
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Bild := INT_TO_BYTE(Bild);
	IF (BildB <> picStandard) AND y THEN
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Bild := INT_TO_BYTE(BildB + 1 - 2 * BOOL_TO_INT(BildB MOD 2 = 1));
	END_IF;

	IF Neustart OR g_bOnlineChange THEN
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].C := FALSE;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].D := FALSE;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].FNr := FNrMan;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Typ := iTyp;
		IF StNr <> '' OR ZylNr <> '' THEN
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtA := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,g_txtA)),15);
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtB := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,g_txtB)),15);
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY := LEFT(CONCAT(CONCAT(StNr,txtVentil),ZylNr),15);
		ELSE
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtA := txtA;
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtB := txtB;
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY := txtVentil;
		END_IF;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtC := '';
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtD := '';
	END_IF;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].A := a;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].B := b;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].on := on;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].man := man;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].enable_man := enable_man;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Fault := err;

	(* Zylinder manuell verfahren *)
	IF 	Zyl_Chains[Zyl_AktModul].aStatus[Zyl_AktChain].Manual THEN
		IF	Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_on AND enable_man THEN
		(* Zylinder manuell einschalten *)
			man := TRUE;
		END_IF;

		(* Zylinder manuell ausschalten *)
		IF 	Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_off AND enable_man THEN
			man := FALSE;
		END_IF;
	ELSE
		man := on;
	END_IF;

	(* Ausgangszuweisung *)
	y := man;

	IF hm_y <> y THEN
		(* Ermittlung der Hübe *)
		Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diStrokes :=
			Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diStrokes + 1;

		(* Zurücksetzen der Verfahrzeiten *)
		bRunning := TRUE;
		IF y THEN
			diTimeToB := 0;
		ELSE
			diTimeToA := 0;
		END_IF;
	END_IF;
END_IF;

(* Zylinder meldet pos wenn manuelle Ansteuerung übereinstimmt *)
(* hier nur für die Berechnung der Zylinderzeit *)
CASE iTyp OF
0:	pos := TRUE;
1:	pos := (NOT y AND a)
		OR (y AND NOT a);
-1:	pos := (NOT y AND NOT b)
		OR (y AND b);
2:	pos := (NOT y AND a AND NOT b)
		OR (y AND NOT a AND b);
END_CASE;

(* Abspeichern der Verfahrzeiten *)
IF bRunning THEN
	IF pos THEN
		IF y THEN
			Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diTimeToB := diTimeToB;
		ELSE
			Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diTimeToA := diTimeToA;
		END_IF;
		bRunning := FALSE;
	ELSE
		IF y THEN
			diTimeToB := diTimeToB + g_diTaskZyklus;
		ELSE
			diTimeToA := diTimeToA + g_diTaskZyklus;
		END_IF;
	END_IF;
END_IF;

hm_y := y;

(* Fehlerfeld initialisieren bei Neustart *)
IF Neustart OR g_bOnlineChange THEN
	IF StNr <> '' OR ZylNr <> '' THEN
		Errors1[1].Txt := LEFT(CONCAT(CONCAT(StNr,txtVentil),CONCAT(ZylNr,'               ')),15);

		tmp := F_ToLCase(g_txtA);
		tmp := CONCAT( tmp, '               ');
		Errors1[2].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		tmp := F_ToLCase(g_txtB);
		tmp := CONCAT( tmp, '               ');
		Errors1[3].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		tmp := F_ToUCase(g_txtA);
		tmp := CONCAT( tmp, '               ');
		Errors1[4].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		tmp := F_ToUCase(g_txtB);
		tmp := CONCAT( tmp, '               ');
		Errors1[5].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		Errors1[4].Nr := FNr;
		Errors1[4].Prio := ErrZyl;
		Errors1[4].ID := ID;

		Errors1[5].Nr := FNr;
		Errors1[5].Prio := ErrZyl;
		Errors1[5].ID := ID;
	ELSE
		Errors1[1].Txt := LEFT(CONCAT(txtVentil,'               '),15);
		Errors1[2].Txt := LEFT(CONCAT(txtA,'               '),15);
		Errors1[3].Txt := LEFT(CONCAT(txtB,'               '),15);
	END_IF;

	Errors1[1].Nr := FNrMan;
	Errors1[1].Prio := ErrZylMan;
	Errors1[1].ID := ID;

	Errors1[2].Nr := FNr;
	Errors1[2].Prio := ErrZyl;
	Errors1[2].ID := ID;

	Errors1[3].Nr := FNr;
	Errors1[3].Prio := ErrZyl;
	Errors1[3].ID := ID;
END_IF;

(*    Zylinderfehler    *)
(*----------------------*)
IF grundbed THEN
	(* Zylinder manuell angesteuert *)
	f1[1] := (on <> man);

	IF NOT	Zyl_Chains[Zyl_AktModul].aStatus[Zyl_AktChain].Manual THEN
		CASE iTyp OF

		1:	(* ZYL1A *)
			(* Fehler A nicht "0" *)
			f1[2] := f1[2] OR (a AND y AND NOT hs1);

			IF StNr <> '' OR ZylNr <> '' THEN
				(* Fehler A nicht "1" *)
				f1[4] := f1[4] OR (NOT a AND NOT y AND NOT hs1);
			ELSE
				(* Fehler A nicht "1" *)
				f1[2] := f1[2] OR (NOT a AND NOT y AND NOT hs1);
			END_IF;

		-1:	(* ZYL1B *)
			(* Fehler B nicht "0" *)
			f1[3] := f1[3] OR (b AND NOT y AND NOT hs1);

			IF StNr <> '' OR ZylNr <> '' THEN
				(* Fehler B nicht "1" *)
				f1[5] := f1[5] OR (NOT b AND y AND NOT hs1);
			ELSE
				(* Fehler B nicht "1" *)
				f1[3] := f1[3] OR (NOT b AND y AND NOT hs1);
			END_IF;

		2:	(* ZYL2 *)
			(* Fehler A nicht "0" *)
			f1[2] := f1[2] OR (a AND b AND y AND NOT hs1);

			(* Fehler B nicht "0" *)
			f1[3] := f1[3] OR (a AND b AND NOT y AND NOT hs1);

			IF StNr <> '' OR ZylNr <> '' THEN
				(* Fehler A nicht "1" *)
				f1[4] := f1[4] OR (NOT a AND NOT y AND NOT hs1);

				(* Fehler B nicht "1" *)
				f1[5] := f1[5] OR (NOT b AND y AND NOT hs1);
			ELSE
				(* Fehler A nicht "1" *)
				f1[2] := f1[2] OR (NOT a AND NOT y AND NOT hs1);

				(* Fehler B nicht "1" *)
				f1[3] := f1[3] OR (NOT b AND y AND NOT hs1);
			END_IF;
		END_CASE;
	END_IF;
END_IF;

(* Freigabe *)
frg := TRUE;
CASE iTyp OF
1:	(* ZYL1A *)
	(* Zylinder hat noch A-Lage *)
	IF a AND y AND NOT hs1 THEN
		frg := FALSE;
	END_IF;

	(* Zylinder erreicht A-Lage nicht *)
	IF NOT a AND NOT y AND NOT hs1 THEN
		frg := FALSE;
	END_IF;

-1:	(* ZYL1B *)
	(* Zylinder erreicht B-Lage nicht *)
	IF NOT b AND y AND NOT hs1 THEN
		frg := FALSE;
	END_IF;

	(* Zylinder hat noch B-Lage *)
	IF b AND NOT y AND NOT hs1 THEN
		frg := FALSE;
	END_IF;

2:	(* ZYL2 *)
	(* Zylinder erreicht B-Lage nicht *)
	(* oder hat noch A-Lage *)
	IF (a OR NOT b) AND y AND NOT hs1 THEN
		frg := FALSE;
	END_IF;

	(* Zylinder erreicht A-Lage nicht *)
	(* oder hat noch B-Lage *)
	IF (b OR NOT a) AND NOT y AND NOT hs1 THEN
		frg := FALSE;
	END_IF;
END_CASE;

IF StNr <> '' OR ZylNr <> '' THEN
	(* Auswertung des Fehlerbausteins *)
	Fehler1(ID:=ID,FAnz:=FAnz,EF:=Errors1,F:=f1,Anzahl:=5);

	err := f1[1] OR f1[2] OR f1[3] OR f1[4] OR f1[5];
ELSE
	(* Auswertung des Fehlerbausteins *)
	Fehler1(ID:=ID,FAnz:=FAnz,EF:=Errors1,F:=f1,Anzahl:=3);

	err := f1[1] OR f1[2] OR f1[3];
END_IF;

(* Ansteuerung von BDE-Online zurücksetzen *)
Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_on := FALSE;
Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_off := FALSE;

(* Keba Panel *)
KebaInitZyl(FNrMan := FNrMan, ZylTyp := 2, txtVentil :=
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY);]]></ST>
    </Implementation>
    <LineIds Name="ZYL_Wechsel">
      <LineId Id="3" Count="289" />
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>