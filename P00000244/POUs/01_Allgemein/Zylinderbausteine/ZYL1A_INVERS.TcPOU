﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6">
  <POU Name="ZYL1A_INVERS" Id="{6db5f728-4d89-4bf7-9d52-b9176ad29fa0}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK ZYL1A_INVERS

(*
Preh IMA AUTOMATION AMBERG GMBH        

<DESC>
Dieser Baustein steuert einen Zylinder mit einer Abfragen (A-Lage) in der Schrittkette an.
Die Zeit für die B-Lage kann übergeben werden. 
Die Zeit startet nachdem die A-Lage verlassen wurde. 
Die Fehler werden lokal erzeugt.
Als StationsID kann die gleiche ID wie die Station hat übertragen werden.
Dieser Baustein ist speziell für Aushubeinheiten gedacht, die in Ruhestellung in der Mittelstellung stehen.
Das Ventil nach oben wird mit einen ZYL1B angesteuert.
Das Ventil nach unten wird mit einen ZYL1A_INVERS angesteuert.
</DESC>

<KEYWORDS>

</KEYWORDS>

<CHANGES>
Änderungen:	

Datum 		|	Version	|	Autor	|	Beschreibung		
-------------------------------------------------------------------------------------------------------------------------------------------------
2005-10-04	|	1.00	|	GST		|	Ursprungsversion	
2011-10-11	|	1.00	|	MBE		|	Integration Keba Panel	
2012-07-16	|	1.00	|	GST		|	VAR_INPUT auf VAR_INPUT PERSISTENT geändert	
2012-12-13	|	2.00	|	SBA		|	Erweiterungen ür neue Betriebsmittelkennzeichnungsrichtlinie: 
			|			|			|	txtVentil, txtSensor, txtA neu, Anz.angezeigter Zeichen von 8 -> 15 erhöht
2013-03-25	|	2.10	|	GST		|	Einführung der globalen Variablen g_txtVentil, g_txtSensor, g_txtA, g_txtB 
2013-04-05	|	2.11	|	GST		|	Stringlänge auf 15 Zeichen mit Left(xxx,15) reduziert
2013-10-09	|	2.12	|	GST		|	Bei Uebergabe des kompletten Fehlertext werden 2 statt 3 Fehler erzeugt
2015-02-05	|	2.20	|	GST		|	Stringlänge reduziert bei txtVentil
2015-04-17	|	2.30	|	MHU		|	Neuer Input-Parameter 'Bild' zur Auswahl der Zylinderbilder für die Schrittkettendiagnose
			|			|			|	Integration von g_bOnlineChange
2016-05-10	|	3.00	|	GST		|	Integration Condition Monitoring
			|			|			|	Übergabe der "Grundbed" anstatt "UZ" mit Default von T#5s
			|			|			|	Übergabe von Sensorverzögerungen TimeA
2016-05-10	|	3.10	|	GST		|	Berechnung der Schaltzeiten auch beim manuellen Betrieb				


</CHANGES>

<VERSION>
3.10
</VERSION>

*)


VAR_INPUT PERSISTENT
	grundbed:	BOOL; 				(* Grundbedingung *)
	a: 			BOOL; 				(* Näherungsschalter Ruhe *)
	enable_man:	BOOL := TRUE;		(* Freigabe der manuellen Zylinderbewegung *)
	on_vz:		TIME := T#100ms;	(* Einschaltzeit *)
	on:			BOOL;				(* Aktueller Zustand des Ventils *)
	FNr:		INT;				(* Fehlernummer *)
	FNrMan:		INT;				(* Fehlernummer für Zylinder manuell angesteuert *)
	FAnz:		INT;				(* Textanzeige *)
	StNr:		STRING(15);			(* Stationsstring *)
	ZylNr:		STRING(15);			(* Zylinderstring *)
	txtVentil:	STRING(15) := g_txtVentil;
									(* Zylinderbezeichnung -
									alte Bezeichnung
											'Y',
									neue Bezeichnung etwa 
											'KH' für Ventil, Wegeventil, Pneumatikventil
											'FL' für Sicherheitsventil
											'GS' für Vakuuminjektor
											'RN' für Druckregelventil
											'RM' für Drosselrückschlagventil
											'SJ' für Druckknopfventil 
									Bezeichnung Zylinder bei Optionaler Beschriftung 'A 128.0'
									*)
	txtA:		STRING(15);			(* Bezeichnung Sensor A nur bei Optionaler Beschriftung 'E 128.0' *)
	ID: 		BYTE; 				(* eindeutige Nummer der Schrittkette *)
	Bild:		TZylPicture := picStandard;	(* Anzuzeigendes Zylinderbild in der Schrittkettendiagnose für Grundstellung *)
	BildB:		TZylPicture := picStandard;	(* Anzuzeigendes Zylinderbild in der Schrittkettendiagnose für Arbeitsstellung *)
	timeUZ:		TIME := T#5s;		(* Überwachungszeit optional *)
	timeA:		TIME := T#0ms;		(* Zeit für Sensor verzögert A optional *)
END_VAR

VAR_OUTPUT PERSISTENT
	y:			BOOL;				(* Zustand Zylinder *)
	pos:		BOOL;				(* Zylinder in Position wie von Kette angesteuert *)
	err:		BOOL;				(* Zylinderfehler *)
	man:		BOOL;				(* Manuelle Ansteuerung des Ventils *)
END_VAR

VAR
	f1: 		ARRAY [1..ErrMaxBits] OF BOOL; 	(* Fehlerbits *)
	Errors1: 	ARRAY[1..ErrMaxBits] OF ERRDAT;	(* Fehlernummer und Text *)
	Fehler1:	ERRORZYL;						(* Instanz zur Fehleranzeige *)
	tmp: 		STRING;							(* Temporärer String *)
	tonUZ:		TON;							(* Überwachungszeit local *)
	t_on:		TON;							(* Timer Einschalten verzögert *)
	tonA:		TON;							(* Timer für Sensor A *)
	posY:		BOOL;							(* Positionsmeldung nur von Y abhängig *)
END_VAR

VAR PERSISTENT
	hm_y:		BOOL;							(* Flankenerkennung Signal "y" *)
	diTimeToA:	DINT;							(* Verfahrzeit -> A *)
	diTimeToB:	DINT;							(* Verfahrzeit -> B *)
	bRunning:	BOOL;							(* Zylinder verfährt *)
END_VAR


]]></Declaration>
    <Implementation>
      <ST><![CDATA[IF Zyl_AktZylinder < Zyl_MaxZylinder THEN
	Zyl_AktZylinder := Zyl_AktZylinder + 1;

	(* Zylinderbild in Schrittkettendiagnose passend anzeigen *)
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Bild := INT_TO_BYTE(Bild);
	IF (BildB <> picStandard) AND y THEN
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Bild := INT_TO_BYTE(BildB + 1 - 2 * BOOL_TO_INT(BildB MOD 2 = 1));
	END_IF;

	IF Neustart OR g_bOnlineChange THEN
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].B := FALSE;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].C := FALSE;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].D := FALSE;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].FNr := FNrMan;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Typ := 1;
		IF StNr <> '' OR ZylNr <> '' THEN
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtA := 
				LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,g_txtA)),15);
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY := 
				LEFT(CONCAT(CONCAT(StNr,txtVentil),ZylNr),15);
		ELSE
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtA := txtA;
			Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY := txtVentil;
		END_IF;
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtB := '';
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtC := '';
		Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtD := '';

		Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diTimeToC := 0;
		Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diTimeToD := 0;
	END_IF;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].A := a;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].on := on;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].man := man;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].enable_man := enable_man;
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].Fault := err;

	(* Zylinder manuell verfahren *)
	IF 	Zyl_Chains[Zyl_AktModul].aStatus[Zyl_AktChain].Manual THEN
		IF	Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_on AND enable_man THEN
		(* Zylinder manuell einschalten *)
			man := TRUE;
		END_IF;

		(* Zylinder manuell ausschalten *)
		IF 	Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_off AND enable_man THEN
			man := FALSE;
		END_IF;
	ELSE
		man := on;
	END_IF;

	y := man;
	IF hm_y <> y THEN
		(* Ermittlung der Hübe *)
		Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diStrokes :=
			Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diStrokes + 1;

		(* Zurücksetzen der Verfahrzeiten *)
		bRunning := TRUE;
		IF y THEN
			diTimeToB := 0;
		ELSE
			diTimeToA := 0;
		END_IF;
	END_IF;
END_IF;

(* Sensoren verzögert *)
tonA(IN := a,PT := timeA);

(* Verzögerungszeiten *)
t_on(IN := NOT a AND NOT on AND NOT man, PT := on_vz);

(* Zylinder meldet pos wenn manuelle Ansteuerung übereinstimmt *)
pos := (on AND man AND a AND tonA.Q)
		OR (NOT on AND NOT man AND NOT a AND t_on.Q);

(* Pos für Zeitbestimmung auch im Manuellen Betrieb *)
posY := (y AND a AND tonA.Q)
		OR (NOT y AND NOT a AND t_on.Q);

(* Abspeichern der Verfahrzeiten *)
IF bRunning THEN
	IF posY THEN
		IF y THEN
			Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diTimeToB := diTimeToB;
		ELSE
			Zyl_Times[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].diTimeToA := diTimeToA;
		END_IF;
		bRunning := FALSE;
	ELSE
		IF y THEN
			diTimeToB := diTimeToB + g_diTaskZyklus;
		ELSE
			diTimeToA := diTimeToA + g_diTaskZyklus;
		END_IF;
	END_IF;
END_IF;

(* Fehlerfeld initialisieren bei Neustart *)
IF Neustart OR g_bOnlineChange THEN
	IF StNr <> '' OR ZylNr <> '' THEN
		Errors1[1].Txt := LEFT(CONCAT(CONCAT(StNr,txtVentil),CONCAT(ZylNr,'               ')),15);

		tmp := F_ToLCase(g_txtA);
		tmp := CONCAT( tmp, '               ');
		Errors1[2].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		tmp := F_ToUCase(g_txtA);
		tmp := CONCAT( tmp, '               ');
		Errors1[3].Txt := LEFT(CONCAT(CONCAT(StNr,g_txtSensor),CONCAT(ZylNr,CONCAT(tmp,'               '))),15);

		Errors1[3].Nr := FNr;
		Errors1[3].Prio := ErrZyl;
		Errors1[3].ID := ID;
	ELSE
		Errors1[1].Txt := LEFT(CONCAT(txtVentil,'               '),15);
		Errors1[2].Txt := LEFT(CONCAT(txtA,'               '),15);
	END_IF;

	Errors1[1].Nr := FNrMan;
	Errors1[1].Prio := ErrZylMan;
	Errors1[1].ID := ID;

	Errors1[2].Nr := FNr;
	Errors1[2].Prio := ErrZyl;
	Errors1[2].ID := ID;
END_IF;

(* Überwachungszeit *)
tonUZ(IN := grundbed AND (hm_y = y),PT := timeUZ);
hm_y := y;

IF tonUZ.Q THEN
	(* Zylinder manuell angesteuert *)
	f1[1] := (on <> man);

	IF NOT	Zyl_Chains[Zyl_AktModul].aStatus[Zyl_AktChain].Manual THEN
		(* Fehler A nicht "0" *)
		f1[2] := f1[2] OR (a AND NOT y);

		IF StNr <> '' OR ZylNr <> '' THEN
			(* Fehler A nicht "1" *)
			f1[3] := f1[3] OR (NOT a AND y);
		ELSE
			(* Fehler A nicht "1" *)
			f1[2] := f1[2] OR (NOT a AND y);
		END_IF;
	END_IF;
END_IF;

IF StNr <> '' OR ZylNr <> '' THEN
	(* Auswertung des Fehlerbausteins *)
	Fehler1(ID:=ID,FAnz:=FAnz,EF:=Errors1,F:=f1,Anzahl:=3);

	err := f1[1] OR f1[2] OR f1[3];
ELSE
	(* Auswertung des Fehlerbausteins *)
	Fehler1(ID:=ID,FAnz:=FAnz,EF:=Errors1,F:=f1,Anzahl:=2);

	err := f1[1] OR f1[2];
END_IF;

(* Ansteuerung von BDE-Online zurücksetzen *)
Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_on := FALSE;
Zyl_Manual[Zyl_AktModul,Zyl_AktChain,Zyl_AktZylinder].Ventil_off := FALSE;

(* Keba Panel *)
KebaInitZyl(FNrMan := FNrMan, ZylTyp := 1, txtVentil :=
	Zyl_Ventile[Zyl_AktModul,Zyl_AktChain][Zyl_AktZylinder].TxtY);]]></ST>
    </Implementation>
    <LineIds Name="ZYL1A_INVERS">
      <LineId Id="3" Count="169" />
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>