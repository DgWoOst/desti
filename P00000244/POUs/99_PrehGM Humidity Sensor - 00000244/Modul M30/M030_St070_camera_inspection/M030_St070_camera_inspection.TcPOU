﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.16">
  <POU Name="M030_St070_camera_inspection" Id="{5ebffced-55e8-47b5-8e11-3c64fb754d68}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK M030_St070_camera_inspection
(*--------------------------------------------------------------------------------------
Preh IMA Automation Amberg GmbH

<DESC>
This station checks if the glueing process was OK.
</DESC>


<CHANGES>
At every change add date, version´s number, abbr. of name and describe change!

Date 		|	Version	|	Author	|	change description		
--------------------------------------------------------------------------------
2017-11-20	|	1.00	|	TTI		|	initial version
2018-02-28	|	1.00	|	PBA		|	reprogramming

</CHANGES>
<VERSION>
1.00
</VERSION>

<DEPENDENCIES>
;
</DEPENDENCIES>



 -- overview of areas for steps --
Steps [000..099]: 	Whatever Block
Steps [100..199]:	Next Whatever Block

  
----------------------------------------------------------------------------------------*)



(* _____________________ *)
(* -- Standard-Inputs -- *)
VAR_INPUT
	OrderNr: 			INT :=1;		(* Nr. Actorder *)
	MoNr: 				INT:= 3;		(* Module-Number *)
	StNr: 				INT:= 070;		(* Stationsnumber *)
	BedienEinheit: 		ZENTR_DATA;		(* Central *)
	FaultID: 			BYTE := 0;		(* ID for Error Array *)
	DisplayNr: 			INT := 1;		(* Number of Textdisplay *)
	MPart: 				INT := 1;		(* Machinepart *)
	in_InitChainTxt:	STRING(39);		(* String for Step Chain Diagnosis *)
	in_sKebaKurz:		STRING(gcKebaStKurzLen) := '1234567890'; (* optional - can also be assigned in initialisation paragraph *)
	in_sKebaBez:		STRING(gcKebaStBezLen) 	:= '11111111112222222222333333333344444444445555555555'; (* optional - can also be assigned in initialisation paragraph *)
END_VAR


(* ___________________ *)
(* -- WPC-Interface -- *)
VAR_INPUT
	in_iWpcAdress		:INT:=31;
END_VAR
VAR_IN_OUT
	io_Statinterface	:FB_StatInfo;
	io_dwWpcFault		:DWORD; // bit array (dword) representing error at wpc-adress
	io_dwWpcWaiting		:DWORD; // bit array (dword) representing waiting at wpc-adress
	io_dwWpcWarning		:DWORD; // bit array (dword) representing warning at wpc-adress
END_VAR

(* ________________________ *)
(* -- Standard-Variables -- *)
VAR PERSISTENT
	SWI: 				BOOL := FALSE;	(* Impulse step is changing forward *)
	SWR:				BOOL := FALSE;	(* Impulse step is changing backwards *)
	Schritt: 			INT := 0;		(* Current step *)
	UZ: 				TON;			(* watchtime *)
	UZ_Fan: 			TON;			(* watchtime for FAN *)
	Fault: 				BOOL;			(* Stations Error *)
	Waiting:			BOOL;			(* Station Waiting *)
	Warning:			BOOL;			(* Station Warning *)
	Betrieb: 			BOOL;			(* Enable Steps *)
	StatEin: 			BOOL;			(* Station on *)
	AllInPos: 			BOOL;			(* all cylinders in position and not actuated manually *)
	Active:				BOOL;			(* stepchain is marked as active *)
	Transport:			BOOL;			(* manual transport is enabled *)
	InitRun:			BOOL;			(* goto homepos is enabled *)
	CycleRun:			BOOL;			(* mode for single cycle is enabled *)
	ManualRun:			BOOL;			(* move cylinders manually is enabled *)
	BackwardsRun:		BOOL;			(* backwards run is enabled *)
	tonSeitSwi:			TON;			(* time since last change of step *)
	tonSeitAllInPos:	TON;			(* time since AllInPos has become TRUE *)
	i:					INT;			(* index-runner for loops *)
	xx:					FB_StepTracker;	(* auto record steps *)
	StatString:			STRING(10);		(* '+MM=SS' *)
	StartCondition:		BOOL;			(* condition for starting station *)
	bStart:				BOOL;			(* station's work on roundtable is running *)
	DelayForCheck:	    TON;			(* time since AllInPos has become TRUE *)
END_VAR


(* ___________________ *)
(* -- Error-Array 1 -- *)
VAR
	Errors1: ARRAY[1..ErrMaxBits] OF ERRDAT := [
	(* Error 01 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/01            '),
	(* Error 02 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/02            '),
	(* Error 03 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/03            '),
	(* Error 04 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/04            '),
	(* Error 05 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/05            '),
	(* Error 06 *)		(Prio := ErrZyl, Nr:= 466, Txt:='070BX1         '),
	(* Error 07 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/07            '),
	(* Error 08 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/08            '),
	(* Error 09 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/09            '),
	(* Error 10 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/10            '),
	(* Error 11 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/11            '),
	(* Error 12 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/12            '),
	(* Error 13 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/13            '),
	(* Error 14 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/14            '),
	(* Error 15 *)		(Prio := ErrZyl, 	Nr:= 6502, Txt:='               '),
	(* Error 16 *)		(Prio := ErrZyl, 	Nr:= 6504, Txt:='               ')];
	f1: ARRAY [1..ErrMaxBits] OF BOOL := [ErrMaxBits(FALSE)];
	Fehler1: ERRORANZ;
	
END_VAR

VAR CONSTANT
	feVisionCheckXtimes		:INT := 6;
	feLcCheckSNRStateFailed:				INT := 15;
	feLcGetRecipeFailed:					INT := 16;
END_VAR

(*
MV,466,*************** §Kamera§ §Prüfung§ §NIO§
MV,466,         §X-Mal in Folge§
MV,3050,*************** §Nach§ §Plasmareinigen§ §Station§ 120/M10
MV,3050,§zu§ §lange§ §Stillstandszeit§ -> §Schlechtteil§		
MV,6504,*************** Line Controller 
MV,6504, getRecipe (OP40) §Fehlgeschlagen§ => §Schlechtteil§
*)


(* _____________________ *)
(* -- Local Variables -- *)
VAR PERSISTENT

	(* == vision sensor Sensopart V20C-CO-A2-W12 == *)
	ixVisionSensorReady				:BOOL:= FALSE;	(* hardware ready *)
	bExtLightON						:BOOL:=FALSE;
	BDE_byteVisionSensorJobNo			:BYTE;			(* Job 1 number for Vision sensor *)
END_VAR


(* _____________________ *)
(* -- Function Blocks -- *)
VAR
(* check glue dosing *)
	fb_VisionSensor					:FB_CamSensopartV10_1;
	awVisionSensorCheckXtimes		:AWFehler;
	
	strEmpfangDatenkanal: STRING;
END_VAR

VAR CONSTANT
	NEST_1							:INT := 01;
	NEST_2							:INT := 02;	
END_VAR

(* -- LineController -- *)
VAR
	LC_Cavity1					:FB_LcStationCommandHandler;
	TypeNo						:STRING(40);
	SerialNo					:STRING(14);
	DataIndex					:UDINT := 1;

END_VAR

VAR PERSISTENT
	OutUserData					:ARRAY[1..100] OF BYTE;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(* ____________________ *)
(* -- Initialisation -- *)
IF neustart OR g_bOnlineChange THEN
	StatString 		:= RIGHT(CONCAT( '000',INT_TO_STRING(StNr) ),3); (* three digits station's prefix is being created automatically for station *)
	in_InitChainTxt := CONCAT( StatString, ' §Camera Inspection§' );
	in_sKebaKurz	:= CONCAT( StatString, 'Camera '); // 3+7 digits
	in_sKebaBez		:= in_InitChainTxt;
END_IF

KebaInitStation(
	Panelnummer		:= gKebaAktPanel,
	Aktiv			:= TRUE,
	Kurzbezeichnung	:= in_sKebaKurz,
	Bezeichnung		:= in_sKebaBez,
	ManuellAktiv	:= TRUE,
	RueckwaertsAktiv:= FALSE,
	GsAktiv			:= FALSE,
);

(* _______________________________________ *)
(* -- Entry for Step Sequence Diagnosis -- *)
InitChain(
	Step 			:= Schritt,
	InitialState 	:= TRUE,
	Fault 			:= Fault,
	Waiting 		:= Waiting,
	Warning 		:= Warning,
	AllInPos 		:= AllInPos,
	RunOpenDoor 	:= TRUE,
	Txt 			:= in_InitChainTxt,
	BedienEinheit 	:= BedienEinheit,
	Active 			:= Active,
	Transport 		:= Transport,
	InitRun 		:= InitRun,
	CycleRun 		:= CycleRun,
	ManualRun 		:= ManualRun,
	BackwardsRun 	:= BackwardsRun);
InitRun := FALSE;


(* _______________ *)
(* -- Parameter -- *)

StatEin 	            	:= ActOrd[MPart].ord_stat[OrderNr].sd_ein;
BDE_byteVisionSensorJobNo	:= DINT_TO_BYTE(LIMIT(1,ActOrd[MPart].ord_stat[E_TAB_MO_30_ST_70].sd_p1, 255));

(* ____________ *)
(* -- Inputs -- *)

ixVisionSensorReady			:=_070_M30_MP1_XD1_POWER_12;

(* _______________________ *)
(* -- Timer / Watchdogs -- *)
UZ( IN:= NOT SWI AND BedienEinheit.grundbed, PT:= T#5s );
tonSeitSwi( IN := NOT SWI AND Bedieneinheit.grundbed, PT := t#30d );
tonSeitAllInPos( IN := AllInPos AND Bedieneinheit.grundbed, PT := T#30D );
DelayForCheck( IN := Schritt=10, PT := T#1S );
UZ_Fan( IN:= bStart, PT:= T#4S );

(* __________________ *)
(* -- Treat Errors -- *)
Fehler1( ID:= FaultID, FAnz:= DisplayNr, EF:= Errors1, F:= f1 );

Fault	:= fb_VisionSensor.Fault;
Warning := fb_VisionSensor.Warning;
Waiting := fb_VisionSensor.Waiting;
FOR i := 1 TO ErrMaxBits BY 1 DO
	IF f1[i] THEN
		IF 		Errors1[i].Prio <= ErrZyl 			THEN Fault := TRUE;
		ELSIF	Errors1[i].Prio <= ErrEndl 			THEN Waiting := TRUE; f1[i] := FALSE;
		ELSIF	Errors1[i].Prio <= ErrWechselpos	THEN Warning := TRUE; f1[i] := FALSE; END_IF
	END_IF
END_FOR


// report Error, Waiting and Warning to cell
IF Fault 	THEN io_dwWpcFault 		:= SETBIT32( io_dwWpcFault,   INT_TO_SINT(in_iWpcAdress) ); END_IF
IF Waiting 	THEN io_dwWpcWaiting 	:= SETBIT32( io_dwWpcWaiting, INT_TO_SINT(in_iWpcAdress) ); END_IF
IF Warning	THEN io_dwWpcWarning 	:= SETBIT32( io_dwWpcWarning, INT_TO_SINT(in_iWpcAdress) ); END_IF

(* Create Errrors / Misc. Errors *)
 
(* _____________________ *)
(* -- Line Controller -- *)
LC_Cavity1.in_Data.OutUserData := ADR(OutUserData);
LC_Cavity1.in_Data.OutUserData_BufferSize := SIZEOF(OutUserData);
LC_Cavity1(
		MoNr				:= MoNr, 
		StNr				:= StNr, 
		FaultID				:= FaultID, 
		DisplayNr			:= DisplayNr, 
		in_strBMK			:= 'LC', 
		in_bConnect			:= BedienEinheit.notaus_ok, 
		in_bEnableLog		:= TRUE, 
		in_sRemoteHost		:= IP_LineController, 
		in_uiRemotePort		:= 30701,//INT_TO_UINT(MoNr * 10000 + StNr + Nest1),
		in_uiStationID		:= 307,
		in_uiStationSubID	:= 1, 
		in_bTelegram64kB	:= FALSE, 
		in_bTypbeschrUebertr:= FALSE
);

(* __________________________________ *)
(* -- Station - Allowed to Operate -- *)
Betrieb := 	Bedieneinheit.betrieb
		AND NOT Fault
		AND NOT ManualRun
		AND BedienEinheit.ventile_ok;

(* ____________________ *)
(* -- Start Station  -- *)
IF 	io_Statinterface.bStartImpuls OR io_statinterface.bBlindStartimp (* PBAIBN *) 
THEN
	StartCondition 
		:= 	StatEin AND	g_WtInfo[io_Statinterface.iWtNr].dataPart.teilestatus = srGood AND g_WtInfo[io_Statinterface.iWtNr].dataPart.StNrLast= 360;					
	IF StartCondition THEN bStart := TRUE; END_IF
END_IF

(* ________________ *)
(* -- Step-Chain -- *)
xx( iStep := Schritt );

(* reset flags *)
fb_VisionSensor.in_bStartMessung := FALSE;
SWI := FALSE;

strEmpfangDatenkanal:=delete(fb_VisionSensor.strEmpfangDatenkanal,27,1);
strEmpfangDatenkanal:=left(strEmpfangDatenkanal,22);
CASE Schritt OF
0:	xx.x := 'in basepos. - Wait For Prework or Station´s Start';
	IF  bStart
	AND BedienEinheit.anl_ein
	AND Betrieb 
	THEN
		Schritt := Schritt + 1;
		SWI := TRUE;
		bExtLightON:=TRUE;
	END_IF	

1: xx.x := 'CheckSerialnumberState (OP15) in LineController';	
	LC_Cavity1.checkSNRState(g_WtInfo[io_Statinterface.iWtNr].dataPart.strTypeNr,g_WtInfo[io_Statinterface.iWtNr].dataPart.strTeileId,0);
	IF LC_Cavity1.comcheckSNRState.bDone THEN
		IF LC_Cavity1.comcheckSNRState.bValid THEN
		
			Schritt := Schritt + 9;
			SWI := TRUE;
		ELSE // Error from LineController
			Errors1[feLcCheckSNRStateFailed].Txt 	:= '          WPC';
			f1[feLcCheckSNRStateFailed] 			:= TRUE;
				
			// Bad Part Line Controller
			g_WtInfo[io_Statinterface.iWtNr].dataPart.teilestatus 			:= srBad;
			g_WtInfo[io_Statinterface.iWtNr].dataPart.StNrSchlechtGesetzt 	:= StNr;		
			g_WtInfo[io_Statinterface.iWtNr].dataPart.iFehlercode			:= bcM30_St070_LcError;
						
			BDEIncCounter( FALSE, bcM30_St070_LcError, 1, ActOrd[MPart].ord_id, MPart);
			BDEIncCounter( TRUE	, bcM30_St070_LcError, 1, ActOrd[MPart].ord_id, MPart);	
		
			Schritt := Schritt + 1;
			SWI 	:= TRUE;				
		END_IF 	
	END_IF
	
2: xx.x := 'error step for line controller';
	IF	 Betrieb
//	AND	AllInPos
	AND NOT f1[feLcCheckSNRStateFailed]
	THEN
		IF 	NOT FALSE
		THEN
			
			Schritt := 0;
			SWI := TRUE;	
			bStart:=FALSE;
		END_IF 
	END_IF
		
10:	xx.x := 'take over - job number for first check';
	fb_VisionSensor.in_byteJobNr := BDE_byteVisionSensorJobNo;
	fb_VisionSensor.in_FileName:=g_WtInfo[io_Statinterface.iWtNr].dataPart.strTeileId;
	IF _005_M30_MP1_WZ3_QM13_MB1 AND DelayForCheck.Q THEN
		(* start vision check *)
		fb_VisionSensor.in_bStartMessung := TRUE;
		Schritt := Schritt + 50;
		SWI := TRUE;
	END_IF	

60:	xx.x := 'start vision sensor check';

	fb_VisionSensor.in_bStartMessung := TRUE;
	IF fb_VisionSensor.out_bFertig
	THEN

		
		IF fb_VisionSensor.Fault    
        //OR (FIND(fb_VisionSensor.strEmpfangDatenkanal,'F') <> 0)   
		//OR (FIND(fb_VisionSensor.strEmpfangDatenkanal,'F') > 8)  
		//OR fb_VisionSensor.arrEmpfangDatenkanal[25]<>80
		OR (FIND(strEmpfangDatenkanal,'F') <> 0)				// check bad part
		THEN
			//BDE Counter
			BDEIncCounter( FALSE, bcM30_St070_VisionSensorCheck, 1, ActOrd[MPart].ord_id, MPart);
			BDEIncCounter( TRUE	, bcM30_St070_VisionSensorCheck, 1, ActOrd[MPart].ord_id, MPart);	
			
			//AW X - times
			awVisionSensorCheckXtimes.ischl := TRUE;
			
			//Write fbDataInfo
				g_WtInfo[io_Statinterface.iWtNr].dataPart.StNrLast					:= StNr;
				g_WtInfo[io_Statinterface.iWtNr].dataPart.teilestatus 				:= srBad;
				g_WtInfo[io_Statinterface.iWtNr].dataPart.StNrSchlechtGesetzt 		:= StNr;		
				g_WtInfo[io_Statinterface.iWtNr].dataPart.iFehlercode				:= bcM30_St070_VisionSensorCheck;
				
				
		  Schritt := Schritt + 5;
		  SWI := TRUE;
	
		ELSE
			awVisionSensorCheckXtimes.igut	 := TRUE;
			
		  Schritt := Schritt + 5;
		  SWI := TRUE;
		END_IF 
		
		
		
		
		
		//Write fbDataInfo
		g_WtInfo[io_Statinterface.iWtNr].dataPart.Mo30_St070.byteVisionSensorJobCheck := fb_VisionSensor.in_byteJobNr;		
	END_IF



(*Step for Line Controller Upload*)	
65: xx.xx:= 'LineController socket ready';
	IF	NOT LC_Cavity1.out_bBusy THEN

		Schritt := Schritt + 1;
		SWI := TRUE;
	END_IF

66:xx.x := 'Upload result to LC';
	IF g_WtInfo[io_Statinterface.iWtNr].dataPart.teilestatus=srGood THEN
		LC_Cavity1.UploadStateAndResultData(	g_WtInfo[io_Statinterface.iWtNr].dataPart.strTypeNr,
										g_WtInfo[io_Statinterface.iWtNr].dataPart.strTeileId,
										0,(*Fail_Location*)
										0(*Fail_Type*)
									);
	ELSE
		LC_Cavity1.UploadStateAndResultData(	g_WtInfo[io_Statinterface.iWtNr].dataPart.strTypeNr,
										g_WtInfo[io_Statinterface.iWtNr].dataPart.strTeileId,
										bcM30_St070_VisionSensorCheck,(*Fail_Location*)
										LCID_FAIL_VISION_CHECK//LCID_FAIL_PRESENCE(*Fail_Type*) 
									);
	END_IF
	IF (LC_Cavity1.comUploadStateAndResultData.bStarted OR LC_Cavity1.comUploadStateAndResultData.bDone) THEN
		Schritt := Schritt + 4;
		SWI := TRUE;
	END_IF;		
	
70:	xx.x := 'End of stepsequence';
	IF Betrieb AND UZ_Fan.Q
	THEN
		bStart := FALSE;
		g_WtInfo[io_Statinterface.iWtNr].dataPart.StNrLast:=MoNr*100+StNr;
		bExtLightON:=FALSE;
		Schritt := 0;
		SWI := TRUE;	
	END_IF
	
END_CASE

(* ___________________________________ *)
(* -- Wrong Colour X-times in a Row -- *)
awVisionSensorCheckXtimes( cntmax 	 := Fehlstop[OrderNr] );
IF awVisionSensorCheckXtimes.fehler THEN f1[feVisionCheckXtimes] := TRUE; END_IF
awVisionSensorCheckXtimes.igut 		 := FALSE;
awVisionSensorCheckXtimes.ischl 	 := FALSE;

(* ____________________ *)
(* -- React to Steps -- *)
_070_M30_MP1_XD1_POWER_9:=bExtLightON OR ActOrd[MPart].ord_stat[OrderNr].sd_z2;
(* ------ Sensopart V10-M30-St070-BX1 ------- *)
fb_VisionSensor(
	MoNr						:= MoNr, 
	BedienEinheit				:= BedienEinheit, 
	FaultID						:= FaultID_VisionSensor_M30_St70,
	DisplayNr					:= DisplayNr, 
	MPart						:= MPart, 
	StatString					:= StatString, 
	in_sNameDevice_10digits		:= '070BX1', 
	in_byteJobNr				:= BDE_byteVisionSensorJobNo,
	in_sRemoteHost				:= IP_M30_ST070_SENSOPART_Camera,
	in_bBetriebsartFreilaufend	:= FALSE, 
	in_bBetriebsartBinaer		:=FALSE,
	in_bBetriebsartASCII		:=TRUE,
	in_bVaryingFileName			:=TRUE,
);

(* ______________________________ *)
(* -- Stop WPC Condition-Based -- *)
IF  Fault
THEN 
	io_Statinterface.bTransportFreigabe := FALSE; 
END_IF

(* _______________________________________ *)
(* -- Report Station is working on WPC  -- *)
io_Statinterface.bLaeuft := bStart;







]]></ST>
    </Implementation>
    <LineIds Name="M030_St070_camera_inspection">
      <LineId Id="167" Count="7" />
      <LineId Id="1901" Count="0" />
      <LineId Id="175" Count="8" />
      <LineId Id="185" Count="20" />
      <LineId Id="541" Count="0" />
      <LineId Id="207" Count="1" />
      <LineId Id="1375" Count="0" />
      <LineId Id="209" Count="0" />
      <LineId Id="1373" Count="0" />
      <LineId Id="1376" Count="0" />
      <LineId Id="213" Count="9" />
      <LineId Id="7770" Count="0" />
      <LineId Id="5392" Count="0" />
      <LineId Id="8137" Count="0" />
      <LineId Id="246" Count="15" />
      <LineId Id="1030" Count="3" />
      <LineId Id="268" Count="2" />
      <LineId Id="6820" Count="17" />
      <LineId Id="6819" Count="0" />
      <LineId Id="271" Count="6" />
      <LineId Id="279" Count="3" />
      <LineId Id="6490" Count="0" />
      <LineId Id="283" Count="1" />
      <LineId Id="287" Count="1" />
      <LineId Id="291" Count="5" />
      <LineId Id="6153" Count="0" />
      <LineId Id="298" Count="0" />
      <LineId Id="7776" Count="0" />
      <LineId Id="299" Count="0" />
      <LineId Id="7777" Count="0" />
      <LineId Id="300" Count="1" />
      <LineId Id="5925" Count="2" />
      <LineId Id="5931" Count="0" />
      <LineId Id="5933" Count="1" />
      <LineId Id="6648" Count="0" />
      <LineId Id="310" Count="0" />
      <LineId Id="6838" Count="0" />
      <LineId Id="6840" Count="31" />
      <LineId Id="6873" Count="3" />
      <LineId Id="6839" Count="0" />
      <LineId Id="1399" Count="0" />
      <LineId Id="5939" Count="1" />
      <LineId Id="7254" Count="0" />
      <LineId Id="5942" Count="2" />
      <LineId Id="5946" Count="1" />
      <LineId Id="3625" Count="0" />
      <LineId Id="4246" Count="0" />
      <LineId Id="5948" Count="2" />
      <LineId Id="5953" Count="0" />
      <LineId Id="5955" Count="1" />
      <LineId Id="7775" Count="0" />
      <LineId Id="7426" Count="1" />
      <LineId Id="7425" Count="0" />
      <LineId Id="7598" Count="0" />
      <LineId Id="7772" Count="0" />
      <LineId Id="5959" Count="8" />
      <LineId Id="5984" Count="3" />
      <LineId Id="7956" Count="3" />
      <LineId Id="5971" Count="2" />
      <LineId Id="7960" Count="2" />
      <LineId Id="5974" Count="0" />
      <LineId Id="6162" Count="0" />
      <LineId Id="7951" Count="2" />
      <LineId Id="7955" Count="0" />
      <LineId Id="5981" Count="2" />
      <LineId Id="6145" Count="0" />
      <LineId Id="4270" Count="0" />
      <LineId Id="6879" Count="0" />
      <LineId Id="6881" Count="24" />
      <LineId Id="6155" Count="0" />
      <LineId Id="6880" Count="0" />
      <LineId Id="6156" Count="2" />
      <LineId Id="6332" Count="0" />
      <LineId Id="7076" Count="0" />
      <LineId Id="6333" Count="0" />
      <LineId Id="6159" Count="1" />
      <LineId Id="6154" Count="0" />
      <LineId Id="6161" Count="0" />
      <LineId Id="332" Count="0" />
      <LineId Id="6168" Count="1" />
      <LineId Id="6167" Count="0" />
      <LineId Id="6171" Count="1" />
      <LineId Id="6175" Count="0" />
      <LineId Id="6173" Count="0" />
      <LineId Id="6170" Count="0" />
      <LineId Id="335" Count="1" />
      <LineId Id="1881" Count="0" />
      <LineId Id="1883" Count="0" />
      <LineId Id="2065" Count="10" />
      <LineId Id="7250" Count="2" />
      <LineId Id="1880" Count="0" />
      <LineId Id="1895" Count="0" />
      <LineId Id="343" Count="2" />
      <LineId Id="2114" Count="0" />
      <LineId Id="346" Count="1" />
      <LineId Id="1897" Count="0" />
      <LineId Id="350" Count="1" />
      <LineId Id="702" Count="0" />
      <LineId Id="4038" Count="1" />
      <LineId Id="5402" Count="0" />
      <LineId Id="4053" Count="0" />
      <LineId Id="4048" Count="0" />
      <LineId Id="4040" Count="0" />
      <LineId Id="3996" Count="1" />
    </LineIds>
  </POU>
</TcPlcObject>