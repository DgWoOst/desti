﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6">
  <POU Name="M40" Id="{8f16c116-8bff-494f-9c04-e5116ecf665d}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK M40
(*--------------------------------------------------------------------------------------
Preh IMA Automation Amberg GmbH

<CHANGES>
Bei jeder Änderung bitte Datum, Versionierung, Namenskürzel und Änderungsbeschreibung eintragen!

Datum 		|	Version	|	Autor	|	Beschreibung		
--------------------------------------------------------------------------------
2017-07-04	|	1.00	|	SBA		|	Initial Version
</CHANGES>
*)

(* _______________________ *)
(* -- Standard-Inputs -- *)
VAR_INPUT
	MoNr:				INT := 1;
	M_Zentrale: 		INT := 1;
	TxtAnzHilfs: 		INT := C_TXT_ANZ_1; 	(* Nr. of Textdisplay *)
	TxtAnzHaupt:		INT := C_TXT_ANZ_M40;	(* Nr. of Textdisplay *)
	MPart:				INT := 1;
	CPart:				INT := 1;	(* Counter-Part *)
	KEBA_PANEL_NR:		INT := PANEL_M40;	
END_VAR


(* _____________________ *)
(* -- Local Variables -- *)
VAR PERSISTENT
	(* == General == *)
	i:								INT;
	z1:								ZAEHLER; (* FB for processing countx *)
	ft_BDE_Laeuft:					F_TRIG;
	dwDebug:						DWORD;
	uz_ps:							ton;		(*DWOCOM*)
	uz_ps_c:						ton;		(*DWOCOM*)
	b_ps:							BOOL;		(*DWOCOM*)

	(* == Lamptest == *)	
	rt_Lampentest:					R_TRIG;
	ton_Lampentest:					TON;

		

	(* == Machine Illumination == *)
	s_Maschinenbeleuchtung:			BOOL:=FALSE;
	rtMaschinenbeleuchtung:			R_TRIG;
	m_bMaschinenbeleuchtung:		BOOL:=FALSE;


	(* == Signal Tower == *)
	Fault,Waiting,Warning:			BOOL:=FALSE;

	
	(* == Central and User Interface == *)
	motorschutz						:BOOL;
	schutzkontrolle					:BOOL;
	schutzstrom						:BOOL;
	hauptstr						:BOOL;
	schutz							:BOOL;

	ztse							:BOOL;
	k_auto							:BOOL;
	k_ueber							:BOOL;
	s_zustimm						:BOOL;

	s_R_Haupt						:BOOL; (* main user control: button reset error (R) as normally open contact button  *)
	s_I_Haupt						:BOOL; (* main user control: button on  (I) as normally open contact button *)
	s_0_Haupt						:BOOL; (* main user control: button off (O) as normally closed contact button *)
	s_HandAuto						:BOOL; (* main user control: manual / auto as button *)
	s_Ueber							:BOOL; (* main user control: ovverride guard 1 = overriden *)
	s_R_Neben						:BOOL; (* secondary user control: button reset error (R) as normally open contact button *)
	s_I_Neben						:BOOL; (* secondary user control: button Ein (I) as normally open contact button *)
	s_0_Neben						:BOOL; (* secondary user control: button Aus (O) as normally closed contact button *)
	s_NotEntr						:BOOL; (* button reset emergency stop *)
	s_Lampentest					:BOOL; (* button lamp test *)
	s_TasterSchutztuer				:BOOL; (* button unlock door *)

	notaus							:BOOL;
	notentr							:BOOL;
	bt_hauptventil					:BOOL;

		
	(* == Roundtable Shiftregisters == *)
	tempRegister1					:DataTeil;
	fbSetCarrierRT100				:FB_Register_SrSetCarrier;
	
	dwRtFreigabe					:ARRAY[RT_M40_100..RT_M40_100] OF DWORD; // flag-field for permission for roundtable to move 
	dwRtStarts						:ARRAY[RT_M40_100..RT_M40_100] OF DWORD; // flag-field for stations working at roundtable
	dwRtFault						:ARRAY[RT_M40_100..RT_M40_100] OF DWORD;
	dwRtWaiting						:ARRAY[RT_M40_100..RT_M40_100] OF DWORD;
	dwRtWarning						:ARRAY[RT_M40_100..RT_M40_100] OF DWORD;

	
	(* == HandOver Shiftregisters == *)
	dwHandOverStarts				: DWORD; // flag-field for stations working at roundtable
	
	(* == Workpiececarrier Circulation Information == *)
	dwWpcFault					:DWORD; // bit array (dword) representing error at wpc-location
	dwWpcWaiting				:DWORD; // bit array (dword) representing waiting at wpc-location
	dwWpcWarning				:DWORD; // bit array (dword) representing warning at wpc-location
	
	bRt100_RtEmpty					:BOOL;
	bRt100_StopRoundTable			:BOOL;
END_VAR


(* _____________________ *)
(* -- Function Blocks -- *)
VAR
	(* == Central == *)	
	Zentrale:							Zentrale_Modul_2015; 				// Modulzentrale
	fbDoorsAndEStop:					FB_DoorsAndEStop;					// Text for Doors/E-Stops
	Temperatur:							Zentrale_NOT_TUER_TEMP_KLIMA_MSS;	// Anzeige Temperaturwächter
	fbBusdiag:							FB_Busdiagnose_237043;				// Busdiagnose
	vofaGefaehrlicheBewegungen:			FB_Festo_VOFA_L26;					// Abschaltung gefährliche Bewegungen 

	(* == Roundtables == *)
	RT_100							:FB_Weiss_EF2;
	fbCycletimeRT_100				:FB_StationszeitenRundtisch;			// Measurement Cycle Time of Roundtable
	
	(* == Stations == *)
	St10_EOLtestwithreferencepart		:Mo40_St010_EOL_test_with_reference_part;
	St70_PickAndPlace					:Mo40_St070_pick_and_place;
	St80_lasermarking					:Mo40_St080_laser_marking;
	St90_outputofpartGripper1			:Mo40_St090_output_of_part_Gripper_1;
	St90_outputofpartGripper2			:Mo40_St090_output_of_part_Gripper_2;
	St90_outputofparthandover			:Mo40_St090_output_of_part_hand_over;
	St90_outputofpartpickandplace		:Mo40_St090_output_of_part_pick_and_place;
	St91_palletmachine					:Mo40_St091_palletizer;
	St100_fixturecleaningandemptyRT4	:Mo40_St100_fixture_cleaning_and_empty_RT4;

	
	(* == Workpiece Carrier Circulation == *)	
	M40_WPC							:M40_WPC;
	St30_FrequencyCheck: INT;
	io_bUpperUnloadPallet: BOOL;
	io_bLowerUnloadPallet: BOOL;
	io_bPalletneedtoUnload: BOOL;
	
END_VAR



(* ____________________ *)
(* -- Error-Array 1 -- *)
VAR
	Errors1: ARRAY[1..ErrMaxBits] OF ERRDAT := [
	(* Error 01 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/01            '),
	(* Error 02 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/02            '),
	(* Error 03 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/03            '),
	(* Error 04 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/04            '),
	(* Error 05 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/05            '),
	(* Error 06 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/06            '),
	(* Error 07 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/07            '),
	(* Error 08 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/08            '),
	(* Error 09 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/09            '),
	(* Error 10 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/10            '),
	(* Error 11 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/11            '),
	(* Error 12 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/12            '),
	(* Error 13 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/13            '),
	(* Error 14 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/14            '),
	(* Error 15 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/15            '),
	(* Error 16 *)		(Prio := ErrZyl, Nr:= 510, Txt:='/16            ')];
	f1: ARRAY [1..ErrMaxBits] OF BOOL := [ErrMaxBits(FALSE)];
	Fehler1: ERRORANZ4;
END_VAR
VAR CONSTANT
END_VAR
(* 
MV,*************** 
*)















(* -- Function Blocks -- *)




]]></Declaration>
    <Implementation>
      <ST><![CDATA[(* _________________________________________ *)
(* Initialisation of Step Sequence Diagnosis *)
InitModul('§Module§ ++M40');


(* _________________________ *)
(* Enable Keba Panel Entries *)
gKebaAktPanel[PANEL_M20] := FALSE;
gKebaAktPanel[PANEL_M30] := FALSE;
gKebaAktPanel[PANEL_M40] := TRUE;



(* _____________ *)
(* Bus Diagnosis *)

fbBusdiag(
	FaultID			:= FaultID_Busdiag,
	DisplayNr		:= TxtAnzHaupt,
	StatString		:= ''	);

	
// turn off all lamps before processing the module 
TurnOffLamps();

(* _____________________ *)
(* Process Safety Inputs *)
 
arrEmergencyClosed[MoNr] := _40NOTZU; 													//IX1010.1	: BOOL; 	(* emergency stop circuit 1 closed *) 

arrDoorCircuitClosed[MoNr] := _40SCHZU1;												//IX1010.3	: BOOL; 	(* safety circuit 1 closed *)

arrDoor[MoNr,1] := _SK1_M40_C501_KF1_Y1;
arrDoor[MoNr,2] := _SK1_M40_C501_KF1_Y2;
arrDoor[MoNr,3] := _SK1_M40_C501_KF1_Y3;
arrDoor[MoNr,4] := _SK1_M40_C501_KF1_Y4;
arrDoor[MoNr,5] := _SK1_M40_C501_KF2_Y1;

(*(* Potential ZTNA *)
arrEmergencyActive[2] := _40NOTAK;													//IX1010.2	: BOOL; 	(* emergency stop circuit potential ZTNA enabled *)
(* Potential ZTSB *)
arrGuardBypass[4,1] := _40ZTSB1;														//IX1010.5	: BOOL; 	(* safety circuit 1 potential ZTSB enabled *) ->MANUAL MODE AND BYPASS
(* Potential ZTSE *)
arrConfirmButton[4,1] := _40ZTSE1;													//IX1011.2	: BOOL; 	(* safety circuit 1 potential ZTSE enabled *) 
(* Potential ZTSC *)
arrDoorCircuitActive[2] := _40ZTSC1;												//IX1010.4	: BOOL; 	(* safety circuit 1 potential ZTSC enabled *)->MANUAL MODE AND BYPASS AND ENABLE BUTTON
*)

(* Potential ZTNA *)
arrEmergencyActive[MoNr] := _40NOTAK;															//add

(* Potential ZTSB *)
arrMainCircuitActive[MoNr] := _40ZTSB1;														//add

(* Doors bridged *)
arrGuardBypass[MoNr,1] := _40KBR1;																//add
	
(* Enabling button *)
arrConfirmButton[MoNr,1] := _40ZTST1;															//add

(* Potential ZTSE *)
arrAktorsEnabled[MoNr] := (_40ZTSB1 AND _40ZTSE1) OR _40ZTSC1;  	//add

(* Doors closed *)
arrDoorCircuitClosed[MoNr] := _40SCHZU1;													//add

(* Potential ZTSC *)
arrDoorCircuitActive[MoNr] := _40ZTSC1;														//add

(* Manualmode activated*)
arrManualActivated[MoNr] := arrManualMode[MoNr] AND arrEmergencyActive[MoNr];					//add

(* Shutter activated*)
arrShutterActivated[MoNr,1] := _080_M40_P1_KF1 AND _080_M40_P1_KF2;					//add



(* ____________________ *)
(* Process User Buttons *)
Zentrale.s_HandAuto	:=
		(	_M40_bCellButtons		AND (BDEButton = cActionM40_TasteHandAuto)	)
	OR	(	_M40_bPanel1			AND gKebaDataToSps[KEBA_PANEL_NR].Bedienung.Taste_HA	);
	
Zentrale.s_R_Haupt		:= 	(	_M40_bPanel1 AND gKebaDataToSps[KEBA_PANEL_NR].Bedienung.Taste_R	);
Zentrale.s_R_Neben		:= 	_GM1_M40_BF1_SF3_14 
						OR 	_GM1_M40_BF3_SF3_14
						OR 	(BDEButton=cActionM40_TasteR);       // Neben    Reset in machine or BDE   ->  can be disable 

IF 			fbBusdiag.bPanelOk                            // bus check ok
	AND 	t_erkenn.Q  																	//delay														
	AND NOT neustart 																	  // not restart
THEN

	Zentrale.s_I_Haupt			:= 	(	_M40_bPanel1 AND gKebaDataToSps[PANEL_M40].Bedienung.Taste_I	)
								OR	(	_M40_bPanel1 AND gKebaDataToSps[PANEL_M40].Bedienung.Taste_Einrichten );
								
	Zentrale.s_0_Haupt			:= 	NOT gKebaDataToSps[PANEL_M40].Bedienung.Taste_0;
	
	Zentrale.s_I_Neben			:= 	_GM1_M40_BF1_SF2_14					
								OR	_GM1_M40_BF3_SF2_14;
																
	Zentrale.s_0_Neben			:= 	_GM1_M40_BF1_SF1					
								AND _GM1_M40_BF3_SF1
								AND (BDEButton<>cActionM40_Taste0);
	
	Zentrale.s_Lampentest		:=	gKebaDataToSps[PANEL_M40].Bedienung.Taste_Lampentest 	
								OR 	(BDEButton = cActionM40_TasteLampentest);
								
	Zentrale.s_TasterSchutztuer	:= 	FALSE;
	
	s_Maschinenbeleuchtung		:= 	(	_M40_bPanel1 AND gKebaDataToSps[PANEL_M40].Bedienung.Taste_F4	) 
								OR 	(	BDEButton = cActionM40_TasteBeleuchtung );
							
								
ELSE
	Zentrale.s_I_Haupt			:= FALSE;
	Zentrale.s_0_Haupt			:= TRUE;

	Zentrale.s_I_Neben			:= FALSE;
	Zentrale.s_0_Neben			:= TRUE;
	
	Zentrale.s_Lampentest		:= FALSE;
	Zentrale.s_TasterSchutztuer	:= FALSE;
	
	s_Maschinenbeleuchtung		:= FALSE;
END_IF



(* _________________ *)
(* -- Temperature -- *)
Temperatur(
	in_b01			:= _SPV_M40_P1_BT1,
	in_b02			:= TRUE,
	in_str01		:= '    =SPV+P1-BT1',
	in_str02		:= '    ',
);


(* __________________ *)
(* -- Treat Errors -- *)
Fehler1( ID:= FaultID_M40, FAnz:= TxtAnzHaupt, EF:= Errors1, F:= f1 );      //   FAnz  act. error message   EF error definitions      F  error field

Fault	:= FALSE;
Warning := FALSE;
Waiting := FALSE;
FOR i := 1 TO ErrMaxBits BY 1 DO
	IF f1[i] THEN
		IF 		Errors1[i].Prio <= ErrZyl 			THEN Fault := TRUE;
		ELSIF	Errors1[i].Prio <= ErrEndl 			THEN Waiting := TRUE; f1[i] := FALSE;
		ELSIF	Errors1[i].Prio <= ErrWechselpos	THEN Warning := TRUE; f1[i] := FALSE; END_IF
	END_IF
END_FOR


(* ______________ *)
(* -- Zentrale -- *)
Zentrale(
	ID						:= FaultID_M40,
	FAnz					:= TxtAnzHaupt,
	NOTZU					:= arrEmergencyClosed[MoNr],
	NOTAK					:= arrEmergencyActive[MoNr],
	ZTSB					:= arrMainCircuitActive[MoNr],
	SCHZU					:= arrDoorCircuitClosed[MoNr],
	ZTSC					:= arrDoorCircuitActive[MoNr],
	ZTSE					:= arrAktorsEnabled[MoNr],	
	TEMP_OK					:= Temperatur.out_bResult,
	KLIMA_OK				:= TRUE,
	VERRIEGELT 				:= mz[M_Zentrale].schutz_ok,
	//	s_R_Haupt			:= look above
	//	s_I_Haupt			:= look above
	//	s_0_Haupt			:= look above
	//	s_HandAuto			:= look above
	s_Ueber					:= arrGuardBypass[MoNr,1],
	in_k_ueber				:= arrGuardBypass[MoNr,1],
	//	s_R_Neben			:= look above
	//	s_I_Neben			:= look above
	//	s_0_Neben			:= look above
	s_pneu_ein_Serv			:= FALSE,
	s_pneu_aus_Serv			:= FALSE,
	s_anl_ein_Serv			:= FALSE,
	s_anl_aus_Serv			:= FALSE,
	s_NotEntr				:= Zentrale.s_I_Haupt,
	//	s_Lampentest		:= look above
	//	s_TasterSchutztuer	:= look above 
	time_Schutztuer			:= T#9S,	// cycle time 5.4s => release time for safety doors has to be greater than 5.4s
	s_rueck					:= FALSE,
	s_zustimm				:= arrConfirmButton[MoNr,1],
	in_k_hand				:= arrManualActivated[MoNr],
	in_VOFA1				:= _GM1_M40_MP1_KZ2_QM1_BG1,
	in_VOFA2				:= _GM1_M40_MP1_KZ2_QM2_BG1,
	in_strVOFA1				:= 'GM1-KZ2-QM1-BG1',
	in_strVOFA2				:= 'GM1-KZ2-QM2-BG1',
	in_strNotZu				:= fbDoorsAndEStop.out_strE_Stop,
	in_strTuer				:= fbDoorsAndEStop.out_strDoor,
	in_strHauptventil		:= '   =GM1-KZ1-QM2',
	in_strDruckwaechter		:= '       =GM1-BP1',
	in_strTemp				:= Temperatur.out_str,
	in_strKlima				:= '               ',

	MSS						:= _MSS_M40,
	in_strMSS				:= ' =PLC+P3-KF10.1',

	in_Druckwaechter		:= _GM1_M40_MP1_KZ1_QM2_BP1,
	in_Hauptventil_ok		:= _GM1_M40_MP1_KZ1_QM2_X1,
	stationsfehler			:= ((FehlerM[MoNr] 		AND 2#11111111111111111111111111111111) <> 0) OR Fault,
	stationsendlagen		:= ((EndlagenM[MoNr] 	AND 2#11111111111111111111111111111111) <> 0) OR Waiting,
	stationswarnungen		:= ((WarnungM[MoNr] 	AND 2#11111111111111111111111111111111) <> 0) OR Warning,
	stationsschutz			:= (SchutzM[MoNr] 		AND 2#11111111111111111111111111111111) <> 0,
	zyklusende				:= (ZyklusEndeM[MoNr] 	AND 2#11111111111111111111111111111111) = 2#11111111111111111111111111111111,
	fehlerstop				:= fbBusdiag.out_bModulStop,
	fehlergrd				:= FALSE,
	
	i_laeuft				:= 	RT_100.out_bStart
							OR	g_arrWtPlace[	cWpcPlaceM40_AtSt010	]._bInitImpulseNextWpc
							OR	g_arrWtPlace[	cWpcPlaceM40_AtSt020	]._bInitImpulseNextWpc
							OR	g_arrWtPlace[	cWpcPlaceM40_AtSt070	]._bInitImpulseNextWpc,

	leerfahren				:= mz[M20_Zentrale].mpart[MPart].hm_leer,
	bDisableNebenRIO		:= FALSE, (* PBAIBN *)
	m_para					:= modul_parameter.Zentr_Struct[M_Zentrale],
	BedienEinheit			:= mz[M_Zentrale],
	out_k_vzschutz			=> arrResetDoorCircuit[MoNr],
	out_hauptventil1		=> _GM1_M40_MP1_KZ1_QM2_MB1,
	out_hauptventil2		=> _GM1_M40_MP1_KZ1_QM2_MB2,
	out_hauptventil_reset	=> _GM1_M40_P1_KF1,
	out_k_NotEntr			=> arrResetEmergency[MoNr],
	out_k_Hand				=> arrManualMode[MoNr],
	out_h_HandAuto			=> ,	// there is none
	//	out_h_I_Haupt		look below
	//	out_h_O_Haupt		look below
	//	out_h_R_Haupt		look below
	//	out_h_I_Neben		look below
	//	out_h_O_Neben		look below
	//	out_h_R_Neben		look below
	out_h_Schutztueren		=> ,	// there is none
	out_h_NotEntr			=> ,	// there is none
	out_h_Rot				=> _GM1_M40_MP1_AF1_PF3,
	out_h_Gelb				=> _GM1_M40_MP1_AF1_PF2,
	out_h_Gruen				=> _GM1_M40_MP1_AF1_PF1,
	//	out_h_Beleuchtung	different solution below
	out_Betriebsstunden		=> _PLC_M40_P1_PG1,
	out_SafetyBridged		=> ,
);
_40VSCHZ1 	:= Zentrale.out_k_Schutztuer;


(* Button Lamp´s (I) *)
_GM1_M40_BF1_SF2_X1 := Zentrale.out_h_I_Neben AND _M40_bCellButtons;                 // ZTSB  (manual and bypass)  _M40_bcellbuttons  is disable 
_GM1_M40_BF3_SF2_X1	:= Zentrale.out_h_I_Neben AND _M40_bCellButtons;

(* Button Lamp´s (R) *)
_GM1_M40_BF1_SF3_X1 := Zentrale.out_h_R_Neben AND _M40_bCellButtons;
_GM1_M40_BF3_SF3_X1	:= Zentrale.out_h_R_Neben AND _M40_bCellButtons;


(* Reset emergency stop *)
_40NAENTR := arrResetEmergency[MoNr];                     					// add

(* Reset door circuit *)
_40VSCHZ1 := arrResetDoorCircuit[MoNr];															//add



(* ____________________________ *)
(* -- Main VOFA Safety Valve -- *)
(*=> Main VOFA is now controlled by .ventile_OK. Sensores are monitored by ZENTRALE! 
(* PZ-Luft mit FB_VOFA_ZTSE geschaltet - Steuerluft- *) *)
_GM1_M40_MP1_KZ2_QM1_MB1 := mz[M_Zentrale].ventile_ok;
_GM1_M40_MP1_KZ2_QM2_MB1 := mz[M_Zentrale].ventile_ok;


// machine illumination
rtMaschinenbeleuchtung( CLK := s_Maschinenbeleuchtung );
IF rtMaschinenbeleuchtung.Q THEN
	m_bMaschinenbeleuchtung := NOT m_bMaschinenbeleuchtung;
END_IF
_SPV_M40_P1_QA1 := m_bMaschinenbeleuchtung;



// Reset Station´s errors
FehlerM[MoNr]	:= 0;
WarnungM[MoNr]	:= 0;
EndlagenM[MoNr]	:= 0;

// use bit 31 from ...[MoNr] from module for wpc-circulation
IF dwWpcFault 	<> 0 THEN FehlerM[	MoNr].31 := 1; dwWpcFault := 0;		END_IF
IF dwWpcWarning <> 0 THEN WarnungM[	MoNr].31 := 1; dwWpcWarning := 0;	END_IF	
IF dwWpcWaiting <> 0 THEN EndlagenM[MoNr].31 := 1; dwWpcWaiting := 0;	END_IF



// Vofa for dangerous movements

vofaGefaehrlicheBewegungen(
	m					:= mz[M_Zentrale],
	FaultID				:= FaultID_M40_VOFA_ZTSC,
	DisplayNr			:= TxtAnzHaupt,
	in_bEnable			:= mz[M_Zentrale].hauptventil AND mz[M_Zentrale].schutz_ok,	(* Pneumatik ein UND Schutz geschlossen*)
	in_bFunktionstest	:= Neustart,
	in_ixDruckwaechter	:= _GM1_M40_MP1_KZ4_QM2_BP1,
	in_sDruck			:= '       -KZ4-BP1',
	in_ixSensor_1		:= _GM1_M40_MP1_KZ3_QM1_BG1,
	in_ixSensor_2		:= _GM1_M40_MP1_KZ3_QM2_BG1,
	in_sVentil_1		:= '       -KZ3-QM1',
	in_sVentil_2		:= '       -KZ3-QM2',
	out_qxVentil_1		=> _GM1_M40_MP1_KZ3_QM1_MB1,
	out_qxVentil_2		=> _GM1_M40_MP1_KZ3_QM2_MB1,
);



// flags for control of guard lock
ZyklusEndeM[MoNr] 	:= 2#11111111111111111111111111111111;
SchutzM[MoNr]		:= 0;


(* reset "working in progress flag" from all station´s after processing info *)
IF StartsM[MoNr]= 0 AND FreigabeM[MoNr]<>16#FFFF_FFFF THEN
	dwDebug:=FreigabeM[MoNr];
END_IF
FreigabeM[MoNr] 	:= 2#11111111111111111111111111111111;

(* Starts Rundschalttisch immer rücksetzen *)
StartsM[MoNr]		:= 0;
 
	

(* _______________________________________________________________ *)
(* --  W O R K P I E C E C A R R I E R   C I R C U L A T I O N  -- *)
M40_WPC(
	MoNr				:= MoNr, 
	BedienEinheit		:= mz[M_Zentrale], 
	DisplayNr			:= TxtAnzHaupt,
	io_dwWpcFault		:= dwWpcFault,
	io_dwWpcWaiting		:= dwWpcWaiting,
	io_dwWpcWarning		:= dwWpcWarning,
);









// /////////////////////////////
// Roundtable Allowed to work //
// /////////////////////////////
// Roundtable 100 /////////////////////////////////////////////////////////////////////////////
bRt100_RtEmpty		:=	(		(arrCarrierData[RT_M40_100][1][1].PartData.teilestatus <= srEmpty)
							AND (arrCarrierData[RT_M40_100][2][1].PartData.teilestatus <= srEmpty)
						    AND (arrCarrierData[RT_M40_100][3][1].PartData.teilestatus <= srEmpty)
							AND (arrCarrierData[RT_M40_100][4][1].PartData.teilestatus <= srEmpty)
						);

bRt100_StopRoundTable := 	bRt100_RtEmpty  AND mz[M_Zentrale].mpart[1].hm_leer;




















(* ____________________ *)
(* -- Roundtable 100 -- *)

RT_100(																			(*FZHCOM*)
	MoNr					:= MoNr,
	StNr					:= 0,
	RtNr					:= RT_M40_100,
	BedienEinheit			:= mz[M_Zentrale],
	FaultID					:= FaultID_M40_RT100,
	DisplayNr				:= TxtAnzHaupt,
	inStrBMK				:= 'TL1', // 15 digits possible
	ixVersorgung 			:= mz[M_Zentrale].schutz_ok,
	ixDrehrichtungUmkehr	:= FALSE,
	in_bFreigabe			:= dwRtFreigabe[RT_M40_100] =  16#FFFF_FFFF AND NOT bRt100_StopRoundTable ,
	in_bStart				:= dwRtStarts[	RT_M40_100]	<> 0,
);

dwRtFreigabe[	RT_M40_100]	:= 16#FFFF_FFFF;
dwRtStarts[		RT_M40_100] := 0;

fbSetCarrierRT100(
	MoNr:= MoNr, 
	StNr:= 41, 
	FaultID:= FaultID_M40_RT100, 
	DisplayNr:= TxtAnzHaupt, 
	RtNr:=RT_M40_100 , 
	TischTyp:= E_OhneZwischenNest, 
	AnzahlAufnahmen:= 4, 
	AufnahmeSet:= 4, 
	CheckAwAufnahme:= , 
	CheckErsteAufnahme:= TRUE, 
	Leerfahren:= mz[MoNr].mpart[Segment_1].hm_leer, 
	impRegisterMove:=RT_100.out_bStart , 
	impCheckAufnahme:=RT_100.out_bStart , 
	in_LastInfoDelete:= FALSE, 
	rse_AwAufnahme:= , 
	str_AwAufnahme:= '', 
	rse_ErsteAufnahme:= _GM1_M40_MP1_BG1, 
	str_ErsteAufnahme:= 'GM1BG1');


(* ________________________________ *)
(* -- Shift Roundtable Registers -- *)
(*
IF RT_100.out_bStart THEN
	// one nests at RT 1 ///////////////////////////////////////
	tempRegister1 					:= g_aRtRegister_M40_100[4];

	g_aRtRegister_M40_100[4]		:= g_aRtRegister_M40_100[3];
	g_aRtRegister_M40_100[3]		:= g_aRtRegister_M40_100[2];
	g_aRtRegister_M40_100[2]		:= g_aRtRegister_M40_100[1];
	
	g_aRtRegister_M40_100[1]	:= tempRegister1;
END_IF

*)

(* _______________________ *)
(* --  S T A T I O N S  -- *)

St10_EOLtestwithreferencepart(
	OrderNr:= E_TAB_MO_40_ST_10, 
	MoNr:= MoNr, 
	StNr:= 10, 
	BedienEinheit:= mz[M_Zentrale], 
	FaultID:=  FaultID_M40_St010, 
	DisplayNr:=TxtAnzHaupt , 
	MPart:=MPart , 
	in_InitChainTxt:= , 
	in_sKebaKurz:= , 
	in_sKebaBez:= , 
	in_iWpcAdress:= cWpcPlaceM40_AtSt010, 
	io_Statinterface:=g_arrStatInfo[cWpcPlaceM40_AtSt010] , 
	io_dwWpcFault:=dwWpcFault , 
	io_dwWpcWaiting:= dwWpcWaiting, 
	io_dwWpcWarning:= dwWpcWarning, 
	io_Statinterface_2:=g_arrStatInfo[cWpcPlaceM40_AtSt020] , 
	io_dwWpcFault_2:=dwWpcFault , 
	io_dwWpcWaiting_2:= dwWpcWaiting, 
	io_dwWpcWarning_2:= dwWpcWarning,
	in_bOffering:= , 
	in_bOfferEnd:= , 
	out_bEnablePrework:= , 
	out_bReadyToTakeOver:= , 
	out_bTakenOver:= 
);

St70_PickAndPlace(
	OrderNr				:=E_TAB_MO_40_ST_70 , 
	MoNr				:= MoNr, 
	StNr				:= 70, 
	BedienEinheit		:= mz[M_Zentrale], 
	FaultID				:= FaultID_M40_St070, 
	DisplayNr			:= TxtAnzHaupt, 
	MPart				:= MPart, 
	in_iWpcAdress		:= cWpcPlaceM40_AtSt070, 
	io_Statinterface	:= g_arrStatInfo[cWpcPlaceM40_AtSt070], 
	io_dwWpcFault		:=dwWpcFault , 
	io_dwWpcWaiting		:=dwWpcWaiting , 
	io_dwWpcWarning		:=dwWpcWarning , 
	RtNr				:= RT_M40_100, 
	RtStNr				:= 1, 
	RtStartimpuls		:= RT_100.out_bStart, 
	io_RtStarts			:= dwRtStarts[RT_M40_100], 
	io_RtFreigabe		:=dwRtFreigabe[RT_M40_100] , 
	io_RtFault			:=dwRtFault[RT_M40_100] , 
	io_RtWaiting		:=dwRtWaiting[RT_M40_100] , 
	io_RtWarning		:= dwRtWarning[RT_M40_100]
);

St80_lasermarking(
	OrderNr:=E_TAB_MO_40_ST_80 , 
	MoNr:= MoNr, 
	StNr:= 80, 
	BedienEinheit:=mz[M_Zentrale] , 
	FaultID:=FaultID_M40_St080 , 
	DisplayNr:=TxtAnzHaupt , 
	MPart:=MPart , 
	in_InitChainTxt:= , 
	in_sKebaKurz:= , 
	in_sKebaBez:= , 
	RtNr:=RT_M40_100 , 
	RtStNr:= 2, 
	RtStartimpuls:= RT_100.out_bStart, 
	io_RtStarts:= dwRtStarts[	RT_M40_100], 
	io_RtFreigabe:= dwRtFreigabe[RT_M40_100], 
	io_RtFault:= dwRtFault[	RT_M40_100], 
	io_RtWaiting:= dwRtWaiting[	RT_M40_100], 
	io_RtWarning:= dwRtWarning[	RT_M40_100], 
);

St90_outputofpartpickandplace(
	OrderNr:= E_TAB_MO_40_ST_90, 
	MoNr:= MoNr, 
	StNr:= 90, 
	BedienEinheit:=mz[M_Zentrale] , 
	FaultID:= FaultID_M40_St090_PNP, 
	DisplayNr:= TxtAnzHaupt, 
	MPart:=MPart , 
	in_InitChainTxt:= , 
	in_sKebaKurz:= , 
	in_sKebaBez:= , 
	RtNr:= RT_M40_100, 
	RtStNr:= 3, 
	RtStartimpuls:= RT_100.out_bStart, 
	io_RtStarts:= dwRtStarts[	RT_M40_100], 
	io_RtFreigabe:= dwRtFreigabe[RT_M40_100], 
	io_RtFault:= dwRtFault[	RT_M40_100], 
	io_RtWaiting:=  dwRtWaiting[	RT_M40_100], 
	io_RtWarning:= dwRtWarning[	RT_M40_100], 
	in_bReadyToTakeOverPart:= St90_outputofpartGripper1.out_bReadyToTakeOverPart OR St90_outputofpartGripper2.out_bReadyToTakeOverPart, 
	out_bTakeOverPart=> , 
	out_bLoadEmptyTray=> , 
);


St90_outputofpartGripper1(
	OrderNr:=E_TAB_MO_40_ST_90 , 
	MoNr:= MoNr, 
	StNr:= 90, 
	BedienEinheit:= mz[M_Zentrale], 
	FaultID:= FaultID_M40_St090_Gripper_1, 
	DisplayNr:=TxtAnzHaupt , 
	MPart:= MPart, 
	in_InitChainTxt:= , 
	in_sKebaKurz:= , 
	in_sKebaBez:= , 
	in_brotationInRight:= St90_outputofparthandover.out_brotationInRight, 
	in_bTakeOverPart:= St90_outputofpartpickandplace.out_bTakeOverPart, 
	in_dataHandling:= St90_outputofpartpickandplace.out_dataHandling, 
	in_HandOverStartimpuls	:=St90_outputofparthandover.out_bStartGripper1,
	in_RobotReady			:=St91_palletmachine.out_bRobotReady,
	io_HandOverStarts:=dwHandOverStarts,
);
out_a4Byte_Robot[2].5:= (St90_outputofpartGripper1.bNgPartEnable OR St90_outputofpartGripper2.bNgPartEnable);			(*FZH need to check*)
out_a4Byte_Robot[2].3:= (St90_outputofpartGripper1.bPartNeedToRT OR St90_outputofpartGripper2.bPartNeedToRT);			(*FZH need to check*)
St90_outputofpartGripper2(
	OrderNr:= E_TAB_MO_40_ST_90, 
	MoNr:= MoNr, 
	StNr:= 90, 
	BedienEinheit:= mz[M_Zentrale], 
	FaultID:= FaultID_M40_St090_Gripper_2, 
	DisplayNr:= TxtAnzHaupt, 
	MPart:= MPart, 
	in_InitChainTxt:= , 
	in_sKebaKurz:= , 
	in_sKebaBez:= , 
	in_brotationInRight:= St90_outputofparthandover.out_brotationInRight, 
	in_bTakeOverPart:= St90_outputofpartpickandplace.out_bTakeOverPart, 
	in_dataHandling:= St90_outputofpartpickandplace.out_dataHandling, 
	in_HandOverStartimpuls	:=St90_outputofparthandover.out_bStartGripper2,
	in_RobotReady			:=St91_palletmachine.out_bRobotReady,
	io_HandOverStarts:=dwHandOverStarts,
);


St90_outputofparthandover(
	OrderNr:=E_TAB_MO_40_ST_90 , 
	MoNr:= MoNr, 
	StNr:= 90, 
	BedienEinheit:= mz[M_Zentrale], 
	FaultID:= FaultID_M40_St090_HandOver, 
	DisplayNr:=TxtAnzHaupt , 
	MPart:= MPart, 
	in_InitChainTxt:= , 
	in_sKebaKurz:= , 
	in_sKebaBez:= , 
	in_dwHandOverStarts:=dwHandOverStarts,
);

St91_palletmachine.in_bUnloadReq:=St90_outputofpartpickandplace.out_bUnloadRequest;
St91_palletmachine(
	OrderNr:= E_TAB_MO_40_ST_91, 
	MoNr:=MoNr , 
	StNr:= 91, 
	BedienEinheit:= mz[M_Zentrale], 
	FaultID:= FaultID_M40_St091, 
	DisplayNr:=TxtAnzHaupt , 
	MPart:=MPart , 
	in_InitChainTxt:= , 
	in_sKebaKurz:= , 
	in_sKebaBez:= , 
	RtNr:=RT_M40_100 , 
	RtStNr:= 3, 
	RtStartimpuls:= RT_100.out_bStart, 
	//io_RtRegister:= g_aRtRegister_M40_100, 
	io_RtStarts:= dwRtStarts[	RT_M40_100], 
	io_RtFreigabe:= dwRtFreigabe[RT_M40_100], 
	io_RtFault:=dwRtFault[	RT_M40_100] , 
	io_RtWaiting:= dwRtWaiting[	RT_M40_100], 
	io_RtWarning:= dwRtWarning[	RT_M40_100] , 
);

St100_fixturecleaningandemptyRT4(
	OrderNr:= E_TAB_MO_40_ST_100, 
	MoNr:= MoNr, 
	StNr:= 100, 
	BedienEinheit:=mz[M_Zentrale] , 
	FaultID:= FaultID_M40_St100, 
	DisplayNr:= TxtAnzHaupt, 
	MPart:= MPart, 
	in_InitChainTxt:= , 
	in_sKebaKurz:= , 
	in_sKebaBez:= , 
	RtNr:= RT_M40_100, 
	RtStNr:= 4, 
	RtStartimpuls:= RT_100.out_bStart, 
	io_RtStarts:= dwRtStarts[	RT_M40_100], 
	io_RtFreigabe:= dwRtFreigabe[RT_M40_100], 
	io_RtFault:=dwRtFault[	RT_M40_100] , 
	io_RtWaiting:= dwRtWaiting[	RT_M40_100], 
	io_RtWarning:= dwRtWarning[	RT_M40_100] ,);

(* _________________________________________ *)
(* -- Measure Cycle Time from Roundtables -- *)
(* - Roundtable 100 - *)
IF neustart OR g_bOnlineChange THEN
	(* 6 Zeichen Pflichtfelder für Stationsbezeichnungen überschreiben *)
	fbCycletimeRT_100.in_arr_sStatBez_6digit[1]  := 'St.70 ';
	fbCycletimeRT_100.in_arr_sStatBez_6digit[2]  := 'St.80 ';
	fbCycletimeRT_100.in_arr_sStatBez_6digit[3]  := 'St.90 ';
	fbCycletimeRT_100.in_arr_sStatBez_6digit[4]  := 'St.100';
END_IF
fbCycletimeRT_100(
	in_dwFreigabeRt			:= dwRtFreigabe[RT_M40_100],
	in_dwInArbeit			:= dwRtStarts[	RT_M40_100],
	in_impSchieb			:= RT_100.in_bStart,
	in_iAnzStat				:= 4,
	);



(* _______________ *)
(* Error Treatment *)
ERROREND(FAnz:= TxtAnzHaupt, Reset:= mz[M_Zentrale].i_reset);



// turn on all lamps if lamp test
IF mz[M_Zentrale].lp_test THEN
	TurnOnLamps();
END_IF;



(*DWOCOM test added pre-stopper*)
uz_ps		(IN:= NOT _IX_1328_1, PT:= T#3S);
uz_ps_c		(IN:= _IX_1328_1, PT:= T#0.5S);

IF uz_ps.Q 
	THEN b_ps:=TRUE;
END_IF

IF uz_ps_c.Q 
	THEN b_ps:=FALSE;
END_IF

_005_M40_MP1_WZ2_QM9_MB1 := b_ps ;
_005_M40_MP1_WZ2_QM9_MB2 := NOT b_ps ;


]]></ST>
    </Implementation>
    <Action Name="TurnOffLamps" Id="{dd728118-8970-4b74-ae3e-0b8f8c2e6acf}">
      <Implementation>
        <ST><![CDATA[_SPV_M40_P1_QA1          		:= FALSE; 	(* lighting equipment on *)

_GM1_M40_MP1_AF1_PF1  		:= FALSE; 	(* signal lamp green *)
_GM1_M40_MP1_AF1_PF2  		:= FALSE; 	(* signal lamp orange *)
_GM1_M40_MP1_AF1_PF3  		:= FALSE; 	(* signal lamp red *)
_GM1_M40_MP1_AF1_4          := FALSE; 	(* signal lamp spare *)
_GM1_M40_MP1_AF1_5          := FALSE; 	(* signal lamp spare *)

_GM1_M40_BF1_SF2_X1         := FALSE; 	(* operating field 1 button lamp (I) *)
_GM1_M40_BF1_SF3_X1         := FALSE; 	(* operating field 1 button lamp (R) *)
_GM1_M40_BF3_SF2_X1         := FALSE; 	(* operating field 1 button lamp (I) *)
_GM1_M40_BF3_SF3_X1         := FALSE; 	(* operating field 1 button lamp (R) *)
]]></ST>
      </Implementation>
    </Action>
    <Action Name="TurnOnLamps" Id="{d9666fdc-7813-4a47-95f5-20031f1a6fb8}">
      <Implementation>
        <ST><![CDATA[_SPV_M40_P1_QA1          		:= TRUE; 	(* lighting equipment on *)

_GM1_M40_MP1_AF1_PF1  		:= TRUE; 	(* signal lamp green *)
_GM1_M40_MP1_AF1_PF2  		:= TRUE; 	(* signal lamp orange *)
_GM1_M40_MP1_AF1_PF3  		:= TRUE; 	(* signal lamp red *)
_GM1_M40_MP1_AF1_4          := TRUE; 	(* signal lamp spare *)
_GM1_M40_MP1_AF1_5          := TRUE; 	(* signal lamp spare *)

_GM1_M40_BF1_SF2_X1         := TRUE; 	(* operating field 1 button lamp (I) *)
_GM1_M40_BF1_SF3_X1         := TRUE; 	(* operating field 1 button lamp (R) *)
_GM1_M40_BF3_SF2_X1         := TRUE; 	(* operating field 1 button lamp (I) *)
_GM1_M40_BF3_SF3_X1         := TRUE; 	(* operating field 1 button lamp (R) *)
]]></ST>
      </Implementation>
    </Action>
    <LineIds Name="M40">
      <LineId Id="2766" Count="92" />
      <LineId Id="3584" Count="19" />
      <LineId Id="2887" Count="226" />
      <LineId Id="4563" Count="6" />
      <LineId Id="4589" Count="5" />
      <LineId Id="4596" Count="1" />
      <LineId Id="4599" Count="0" />
      <LineId Id="4601" Count="0" />
      <LineId Id="4607" Count="1" />
      <LineId Id="4570" Count="1" />
      <LineId Id="4582" Count="6" />
      <LineId Id="4572" Count="9" />
      <LineId Id="3114" Count="76" />
      <LineId Id="4802" Count="2" />
      <LineId Id="4801" Count="0" />
      <LineId Id="3191" Count="90" />
      <LineId Id="4372" Count="0" />
      <LineId Id="3282" Count="18" />
      <LineId Id="4373" Count="0" />
      <LineId Id="3301" Count="16" />
      <LineId Id="4997" Count="0" />
      <LineId Id="3318" Count="72" />
      <LineId Id="4182" Count="0" />
      <LineId Id="3391" Count="1" />
      <LineId Id="3984" Count="8" />
      <LineId Id="3983" Count="0" />
      <LineId Id="3793" Count="0" />
      <LineId Id="3792" Count="0" />
      <LineId Id="3393" Count="2" />
      <LineId Id="9" Count="0" />
    </LineIds>
    <LineIds Name="M40.TurnOffLamps">
      <LineId Id="2" Count="11" />
      <LineId Id="1" Count="0" />
    </LineIds>
    <LineIds Name="M40.TurnOnLamps">
      <LineId Id="2" Count="6" />
      <LineId Id="20" Count="0" />
      <LineId Id="10" Count="3" />
      <LineId Id="1" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>