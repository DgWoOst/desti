﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6">
  <POU Name="Mo40_St090_output_of_part_hand_over" Id="{90f5ae79-3e48-4fd9-911f-cd985fec54d8}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK Mo40_St090_output_of_part_hand_over
(*--------------------------------------------------------------------------------------
Preh IMA Automation Amberg GmbH

<DESC>
This station was to unloading the finished parts of M30. First vision sensor check the parts. Then the vertical cylinder moves down and the gripper catch one parts on the fixture of the RT. The
horizontal cylinder extends out to the turn over position. Then the vertical cylinder moves down again to release the parts on the gripper of turn over position.Then the horizontal cylinder
continue to move to the loading position. In the same time, The rotary cylinder turns 180 degree and the RT turn 90 degree . Then second parts put on the gripper of turn over position . If
the parts was good,The robort gripper will catch it to the tray. If the parts was bad,The robort gripper will catch it to the NOK-BOX. At last all the cylinder will be back to the home position
and the RT turn 90 degree.
</DESC>


<CHANGES>
At every change add date, version´s number, abbr. of name and describe change!

Date 		|	Version	|	Author	|	change description		
--------------------------------------------------------------------------------
2017-10-27	|	1.00	|	FZH		|	initial version

</CHANGES>
<VERSION>
1.00
</VERSION>

<DEPENDENCIES>
;
</DEPENDENCIES>



 -- overview of areas for steps --
Steps [000..099]: 	Whatever Block
Steps [100..199]:	Next Whatever Block

  
----------------------------------------------------------------------------------------*)



(* _____________________ *)
(* -- Standard-Inputs -- *)
VAR_INPUT
	OrderNr: 			INT :=1;		(* Nr. Actorder *)
	MoNr: 				INT:= 1;		(* Module-Number *)
	StNr: 				INT:= 1;		(* Stationsnumber *)
	BedienEinheit: 		ZENTR_DATA;		(* Central *)
	FaultID: 			BYTE := 0;		(* ID for Error Array *)
	DisplayNr: 			INT := 1;		(* Number of Textdisplay *)
	MPart: 				INT := 1;		(* Machinepart *)
	in_InitChainTxt:	STRING(39);		(* String for Step Chain Diagnosis *)
	in_sKebaKurz:		STRING(gcKebaStKurzLen) := '1234567890'; (* optional - can also be assigned in initialisation paragraph *)
	in_sKebaBez:		STRING(gcKebaStBezLen) 	:= '11111111112222222222333333333344444444445555555555'; (* optional - can also be assigned in initialisation paragraph *)
//	in_a4Byte_Robot 	AT %I*: ARRAY [0..3] OF BYTE;
END_VAR

(* ====== IF NO FEEDING STATION EXISTS PLEASE DELETE THIS PARAGRAPH AND ELEMINATE SUBSEQUENT ERRORS ====== *)
(* ____________________________ *)
(* -- Handshake with Feeding -- *)
VAR_INPUT PERSISTENT

	in_bTakeOverPart		:BOOL:=FALSE;
	in_dataHandling			:DataTeil;
	in_dwHandOverStarts		: DWORD; // flag-field for stations working at HandOver
END_VAR
VAR_OUTPUT PERSISTENT
	out_brotationInRight 		:BOOL:=FALSE;
	out_bStartGripper1			:BOOL;
	out_bStartGripper2			:BOOL;
END_VAR
VAR_IN_OUT PERSISTENT
END_VAR


VAR
	bRobotDriveReady     		:BOOL:=FALSE;
	bRobotSafetyPos  			:BOOL:=FALSE;
END_VAR


(* ====== IF NO FEEDING STATION EXISTS PLEASE DELETE THIS PARAGRAPH AND ELEMINATE SUBSEQUENT ERRORS ====== *)




(* ________________________ *)
(* -- Standard-Variables -- *)
VAR PERSISTENT
	SWI: 				BOOL := FALSE;	(* Impulse step is changing forward *)
	SWR:				BOOL := FALSE;	(* Impulse step is changing backwards *)
	Schritt: 			INT := 0;		(* Current step *)
	UZ: 				TON;			(* watchtime *)
	Fault: 				BOOL;			(* Stations Error *)
	Waiting:			BOOL;			(* Station Waiting *)
	Warning:			BOOL;			(* Station Warning *)
	Betrieb: 			BOOL;			(* Enable Steps *)
	StatEin: 			BOOL;			(* Station on *)
	AllInPos: 			BOOL;			(* all cylinders in position and not actuated manually *)
	Active:				BOOL;			(* stepchain is marked as active *)
	Transport:			BOOL;			(* manual transport is enabled *)
	InitRun:			BOOL;			(* goto homepos is enabled *)
	CycleRun:			BOOL;			(* mode for single cycle is enabled *)
	ManualRun:			BOOL;			(* move cylinders manually is enabled *)
	BackwardsRun:		BOOL;			(* backwards run is enabled *)
	tonSeitSwi:			TON;			(* time since last change of step *)
	tonSeitAllInPos:	TON;			(* time since AllInPos has become TRUE *)
	i:					INT;			(* index-runner for loops *)
	xx:					FB_StepTracker;	(* auto record steps *)
	StatString:			STRING(10);		(* '+MM=SS' *)
	StartCondition:		BOOL;			(* condition for starting station *)
	bStart:				BOOL;			(* station's work on roundtable is running *)
	NestNr:				INT;			(* number of nest within fixation *)
END_VAR


(* ___________________ *)
(* -- Error-Array 1 -- *)
VAR
	Errors1: 			ARRAY[1..ErrMaxBits] OF ERRDAT := [
	(* Error 01 *)		(Prio := ErrZyl,  Nr:= 510, Txt:='/01            '),
	(* Error 02 *)		(Prio := ErrZyl,  Nr:= 510, Txt:='/02            '),
	(* Error 03 *)		(Prio := ErrZyl,  Nr:= 510, Txt:='/03            '),
	(* Error 04 *)		(Prio := ErrZyl,  Nr:= 510, Txt:='/04            '),
	(* Error 05 *)		(Prio := ErrEndl, Nr:= 5020,Txt:='St.90 Handover '), //*************** Wait the Pick and Place Ready Signal
	(* Error 06 *)		(Prio := ErrEndl, Nr:= 5021,Txt:='St.90 Handover '), //*************** Wait the Pick and Place Safety Pos Signal
	(* Error 07 *)		(Prio := ErrEndl, Nr:= 510, Txt:='/07            '),
	(* Error 08 *)		(Prio := ErrEndl, Nr:= 510, Txt:='/08            '),
	(* Error 09 *)		(Prio := ErrEndl, Nr:= 510, Txt:='/09            '),
	(* Error 10 *)		(Prio := ErrZyl,  Nr:= 510, Txt:='/10            '),
	(* Error 11 *)		(Prio := ErrZyl,  Nr:= 510, Txt:='/11            '),
	(* Error 12 *)		(Prio := ErrZyl,  Nr:= 510, Txt:='/12            '),
	(* Error 13 *)		(Prio := ErrZyl,  Nr:= 510, Txt:='/13            '),
	(* Error 14 *)		(Prio := ErrZyl,  Nr:= 510, Txt:='/14            '),
	(* Error 15 *)		(Prio := ErrZyl,  Nr:= 510, Txt:='/15            '),
	(* Error 16 *)		(Prio := ErrWechselpos, Nr:= 510, Txt:='/16            ')];
	f1				: ARRAY [1..ErrMaxBits] OF BOOL := [ErrMaxBits(FALSE)];
	Fehler1			: ERRORANZ;
	_10_1Y1B		: INT;
	zylAbteilenQuer	: INT;
	on: INT;
END_VAR




(* _____________________ *)
(* -- Local Variables -- *)
VAR PERSISTENT

	(* == Cylinders == *)	
	zyl2_Hand_over_rotating3			:ZYL2; (* MM8 *)
	ixHand_over_rotating3A				:BOOL:=FALSE;
	ixHand_over_rotating3B				:BOOL:=FALSE;
	
	
  	dataHandling_1						:DataTeil;
	dataHandling_2						:DataTeil;
	
	rotation_in_right 					:BOOL:=FALSE;
	hm_HandOverRotating3On						:BOOL:=FALSE;
END_VAR





(* _____________________ *)
(* -- Function Blocks -- *)
VAR
//	Mo40_St090_output_of_part_pick_and_place: Mo40_St090_output_of_part_pick_and_place;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[(* ____________________ *)
(* -- Initialisation -- *)
IF neustart OR g_bOnlineChange THEN
	StatString 			:= RIGHT(CONCAT( '000',INT_TO_STRING(StNr) ),3); (* three digits station's prefix is being created automatically for station *)
	in_InitChainTxt 	:= CONCAT( StatString, ' §Hand Over§' );
	in_sKebaKurz		:= CONCAT( StatString, 'HO'); // 3+7 digits
	in_sKebaBez			:= in_InitChainTxt;
END_IF

KebaInitStation(
	Panelnummer			:= gKebaAktPanel,
	Aktiv				:= TRUE,
	Kurzbezeichnung		:= in_sKebaKurz,
	Bezeichnung			:= in_sKebaBez,
	ManuellAktiv		:= TRUE,
	RueckwaertsAktiv	:= FALSE,
	GsAktiv				:= FALSE,
);

(* _______________________________________ *)
(* -- Entry for Step Sequence Diagnosis -- *)
InitChain(
	Step 				:= Schritt,
	InitialState 		:= TRUE,
	Fault 				:= Fault,
	Waiting 			:= Waiting,
	Warning 			:= Warning,
	AllInPos 			:= AllInPos,
	RunOpenDoor 		:= TRUE,
	Txt 				:= in_InitChainTxt,
	BedienEinheit 		:= BedienEinheit,
	Active 				:= Active,
	Transport 			:= Transport,
	InitRun 			:= InitRun,
	CycleRun 			:= CycleRun,
	ManualRun 			:= ManualRun,
	BackwardsRun 		:= BackwardsRun);
	InitRun 			:= FALSE;
(* _______________ *)
(* -- Parameter -- *)
StatEin 				:= ActOrd[MPart].ord_stat[OrderNr].sd_ein;

(* ____________ *)
(* -- Inputs -- *)
bRobotDriveReady		:=in_a4Byte_Robot[0].5 ;			//FZHCOM
bRobotSafetyPos			:=in_a4Byte_Robot[1].0 ;			//FZHCOM

ixHand_over_rotating3A:=_090_M40_MP1_BG9_A;
ixHand_over_rotating3B:=_090_M40_MP1_BG9_B;

(* _______________________ *)
(* -- Timer / Watchdogs -- *)
UZ( IN:= NOT SWI AND BedienEinheit.grundbed, PT:= T#5S );
tonSeitSwi( IN := NOT SWI AND Bedieneinheit.grundbed, PT := t#30d );
tonSeitAllInPos( IN := AllInPos AND Bedieneinheit.grundbed, PT := T#30D );
(* _______________ *)
(* -- Cylinders -- *)

zyl2_Hand_over_rotating3(
	grundbed:= Bedieneinheit.grundbed, 
	a:= ixHand_over_rotating3A, 
	b:= ixHand_over_rotating3B, 
	enable_man:=Bedieneinheit.ventile_ok, 
	FNr:= 207,
	FNrMan:= 607, 
	FAnz:= DisplayNr, 
	StNr:= StatString, 
	ZylNr:= '8', 
	ID:= FaultId, 
	Bild:=picDrehmodul1_0Grad1 , 
);		

AllInPos :=	zyl2_Hand_over_rotating3.pos;
	
				
(* __________________ *)
(* -- Treat Errors -- *)
Fehler1( ID:= FaultID, FAnz:= DisplayNr, EF:= Errors1, F:= f1 );
Fault	:=	zyl2_Hand_over_rotating3.err
			(* Error´s of function blocks *);
				
Warning := FALSE;
Waiting := FALSE;
FOR i := 1 TO ErrMaxBits BY 1 DO
	IF f1[i] THEN
		IF 	Errors1[i].Prio <= ErrZyl 					THEN Fault := TRUE;
			ELSIF	Errors1[i].Prio <= ErrEndl 			THEN Waiting := TRUE; f1[i] := FALSE;
			ELSIF	Errors1[i].Prio <= ErrWechselpos	THEN Warning := TRUE; f1[i] := FALSE; 
		END_IF
	END_IF
END_FOR

(* __________________________________ *)
(* -- Station - Allowed to Operate -- *)
Betrieb := 	Bedieneinheit.betrieb
		AND NOT Fault
		AND NOT ManualRun
		AND BedienEinheit.ventile_ok;
		
(* ________________ *)
(* -- Step-Chain -- *)
xx( iStep := Schritt );
(* reset flags *)
SWI := FALSE;
out_bStartGripper1:=FALSE;
out_bStartGripper2:=FALSE;
CASE Schritt OF
0:	xx.x := 'in basepos. - wait FOR prework OR station´S start';
	IF  AllInPos
		AND BedienEinheit.anl_ein
		AND Betrieb THEN
		Schritt := Schritt + 10;
		SWI := TRUE;
	END_IF

10:	xx.x := 'set or reset rotation in right signal';
	IF AllInPos
 	AND Betrieb THEN
		IF  hm_HandOverRotating3On  THEN
			out_brotationInRight:=TRUE;
		ELSIF NOT hm_HandOverRotating3On  THEN
			out_brotationInRight:=FALSE;
		END_IF
		Schritt := Schritt + 10;
		SWI := TRUE;
	END_IF
	
20:xx.x:='set the gripper sequence start signal';
	IF AllInPos
 	AND Betrieb THEN
		out_bStartGripper1:=TRUE;
		out_bStartGripper2:=TRUE;
		Schritt := Schritt + 10;
		SWI := TRUE;
	END_IF

30: xx.x:='check robot signal and sequence_2 no start signal';    
	IF AllInPos
  	AND Betrieb  THEN
		IF in_dwHandOverStarts=0 THEN
			Schritt := Schritt + 10;
			SWI := TRUE;
		END_IF
	END_IF
40:	xx.x := 'good part Hand over Rotating ->ON (0/180)';
	IF AllInPos
 	AND Betrieb THEN
		IF	NOT hm_HandOverRotating3On THEN
			hm_HandOverRotating3On:=TRUE;
		ELSE
			hm_HandOverRotating3On:=FALSE;
		END_IF
		Schritt := Schritt + 10;
		SWI := TRUE;
	END_IF

50:xx.x:='wait the rotating cylinder finished';
	IF AllInPos
 	AND Betrieb THEN
		Schritt := 0;
		SWI := TRUE;
	END_IF
END_CASE

(* ____________________ *)
(* -- React to Steps -- *)

IF NOT ManualRun THEN
  	zyl2_Hand_over_rotating3.on 	:= 	hm_HandOverRotating3On;
END_IF

(* _________________ *)
(* -- Set outputs -- *)

IF ManualRun THEN (* assign cylinderoutputs .y (by "AND Bedieneinheit.ventile_ok") *)

	_090_M40_MP1_WZ5_QM8_MB1	:= 		zyl2_Hand_over_rotating3.y	AND Bedieneinheit.ventile_ok;
	_090_M40_MP1_WZ5_QM8_MB2	:=	NOT	zyl2_Hand_over_rotating3.y	AND Bedieneinheit.ventile_ok;

ELSE (* assign cylinderoutputs .on (by "AND Bedieneinheit.ventile_ok") *)

	_090_M40_MP1_WZ5_QM8_MB1	:= 		zyl2_Hand_over_rotating3.on	AND Bedieneinheit.ventile_ok;
	_090_M40_MP1_WZ5_QM8_MB2	:=	NOT	zyl2_Hand_over_rotating3.on	AND Bedieneinheit.ventile_ok;
END_IF
]]></ST>
    </Implementation>
    <LineIds Name="Mo40_St090_output_of_part_hand_over">
      <LineId Id="1194" Count="90" />
      <LineId Id="1560" Count="0" />
      <LineId Id="1287" Count="11" />
      <LineId Id="1734" Count="0" />
      <LineId Id="1299" Count="78" />
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>