﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6">
  <POU Name="FB_IfmDte101" Id="{7f6bc668-42ae-41c5-908b-4e0bf81da260}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_IfmDte101
(*--------------------------------------------------------------------------------------
IMA AUTOMATION AMBERG GMBH        

Bei jeder Änderung bitte Datum, Versionierung, Namenskürzel und Änderungsbeschreibung eintragen!

Datum 		|	Version	|	Autor	|	Beschreibung		
--------------------------------------------------------------------------------
2014-02-15	|	1.00	|	SBA		|	Ursprungsversion	
2016-02-03	|	1.01	|	SBA		|	Anpassung an DK-42234947
2016-08-10	|	1.02	|	SBA		|	Änderung von zweidimensionalen Ablagearray auf eindimensional
2017-07-13	|	2.00	|	SBA		|	Portierung von DTE100 (Profibus) auf DTE101 (Profinet)
			|			|			|
			

Treiberbaustein zum ansprechen eines Kanales für einen 4-Kanal Idensystemleser von
IFM mit der Typbezeichnung DTE 100.

Das System basiert auf 13,56 MHz Übertragungsfrequenz.

Derzeit ist die Schnittstelle für "RWH UID 20 Bytes"-Betriebsart umgesetzt.
Das ermöglicht eine UID von 8 Bytes.
Möglich wären bei einer einfachen Erweiterung des Bausteins 
			"RWH UID 15 Bytes" (12 Bytes Nutzdaten für UID)
	oder	"RWH UID 19 Bytes" (16 Bytes Nutzdaten für UID).
	
Der Controller beherrscht über die Betriebsart "RWH UID xx" das zyklische
automatische lesen des Datenträgers sowie das Beschreiben eines Datenträgers.


Schritt  [   0 .. 20 ]:		Zyklisches Lesen + Sprungverteiler für Handshake zu Lesen und Schreiben
Schritte [  50 .. 99 ]: 	Schreiben des Datenträgers
Schritte [ 100 ..130 ]: 	Lesen des Datenträgers
*)


(* ________________________________ *)
(* -- Standard-Eingangsvariablen -- *)
VAR_INPUT
	MoNr:				INT := 1;
	BedienEinheit:		ZENTR_DATA;		(* Zentrale *)
	FaultID: 			BYTE := 0;		(* ID für Fehlerfeld *)
	DisplayNr: 			INT := 1;		(* Nummer der Textanzeige *)
	StatString: 		STRING(20);		(* Stationsstring *)
END_VAR


(* _______________________________ *)
(* -- Schnittstelle mit Automat -- *)
VAR_INPUT PERSISTENT
	in_sBetriebsmittelname :STRING(15) := '10-1B105       ';

	in_bChInUse			:BOOL := TRUE;
		(* Parametrierung, ob Kanal in Gebrauch (ob Auswertung erfolgen soll) *)

	in_bWriteStart		:BOOL := FALSE;
	in_bReadStart		:BOOL := FALSE;
	in_WtNrMin			:INT  := 1;
	in_WtNrMax			:INT  := g_maxNumWt;
END_VAR
VAR_OUTPUT PERSISTENT
	out_bWriteDone		:BOOL := FALSE;
	out_bIsWriting		:BOOL := false;
	
	out_bReadDone		:BOOL := FALSE;

	out_iUidRead		:INT;
END_VAR


(* ____________________________ *)
(* -- Hardware-Schnittstelle -- *)
VAR_INPUT PERSISTENT
	iwRwhUid_20Bytes	AT %I*	:ARRAY[1..20] OF BYTE;
		(* Hardware-Eingang für Kanäle 1-4 *)
END_VAR
VAR_OUTPUT
	qwRwhUid_20Bytes	AT %Q*	:ARRAY[1..20] OF BYTE;
		(* Hardware-Ausgänge für Kanäle 1-4 *)
END_VAR


(* ________________________ *)
(* -- Standard-Variablen -- *)
VAR PERSISTENT
	SWI 		:BOOL 	:= FALSE;	(* Schrittwechselimpuls vorwärts *)
	Schritt 	:INT 	:= 0;		(* Aktueller Schritt *)
	UZ 			:TON;				(* Überwachungszeit *)
	Fault 		:BOOL;				(* Stationsfehler *)
	Waiting		:BOOL;				(* Station wartet *)
	Warning		:BOOL;				(* Warnung steht an *)
	Betrieb		:BOOL;				(* Betriebsmerker *)
END_VAR


(* ____________________ *)
(* -- Fehler-Array 1 -- *)
VAR
	Errors1: ARRAY[1..ErrMaxBits] OF ERRDAT := [
	(* Fehler 01 *)		(Prio := ErrZyl,  Nr:= 801, Txt:='in_sBetriebsmn.'),
	(* Fehler 02 *)		(Prio := ErrZyl,  Nr:= 802, Txt:='in_sBetriebsmn.'),
	(* Fehler 03 *)		(Prio := ErrZyl,  Nr:= 803, Txt:='in_sBetriebsmn.'),
	(* Fehler 04 *)		(Prio := ErrZentr,Nr:= 804, Txt:='in_sBetriebsmn.'),
	(* Fehler 05 *)		(Prio := ErrZyl,  Nr:= 805, Txt:='in_sBetriebsmn.'),
	(* Fehler 06 *)		(Prio := ErrZyl,  Nr:= 806, Txt:='in_sBetriebsmn.'),
	(* Fehler 07 *)		(Prio := ErrZyl,  Nr:= 807, Txt:='in_sBetriebsmn.'),
	(* Fehler 08 *)		(Prio := ErrZyl,  Nr:= 808, Txt:='in_sBetriebsmn.'),
	(* Fehler 09 *)		(Prio := ErrZyl,  Nr:= 809, Txt:='in_sBetriebsmn.'),
	(* Fehler 10 *)		(Prio := ErrZyl,  Nr:= 810, Txt:='in_sBetriebsmn.'),
	(* Fehler 11 *)		(Prio := ErrZyl,  Nr:= 811, Txt:='in_sBetriebsmn.'),
	(* Fehler 12 *)		(Prio := ErrZyl,  Nr:= 812, Txt:='in_sBetriebsmn.'),
	(* Fehler 13 *)		(Prio := ErrZyl,  Nr:= 813, Txt:='in_sBetriebsmn.'),
	(* Fehler 14 *)		(Prio := ErrZyl,  Nr:= 814, Txt:='in_sBetriebsmn.'),
	(* Fehler 15 *)		(Prio := ErrZyl,  Nr:= 815, Txt:='in_sBetriebsmn.'),
	(* Fehler 16 *)		(Prio := ErrZyl,  Nr:= 816, Txt:='in_sBetriebsmn.')];
	f1: ARRAY [1..ErrMaxBits] OF BOOL := [ ErrMaxBits(FALSE) ];
	Fehler1: ERRORANZ;
END_VAR
VAR CONSTANT
	feLesenAwFehlt				:INT := 01;
	feLesenTimeout				:INT := 02;
	feLesenDatenLeer			:INT := 03;
	feSchreibenAwFehlt			:INT := 04;
	feSchreibenTimeout			:INT := 05;
	feSchreibenGegenlesenNio	:INT := 06;
	feFalscherWtTypGefunden		:INT := 07;
END_VAR


(*
MV,801,*************** IFM-§Identsystem§
MV,801,§Anwesenheit§ §Datenträger§ §fehlt§ §bei Lesen§

MV,802,*************** IFM-§Identsystem§
MV,802,§Timeout§ §bei Lesen§ §Datenträger§

MV,803,*************** IFM-§Identsystem§
MV,803,§keine Daten§ §auf§ §Datenträger§ §gespeichert§

MV,804,*************** IFM-§Identsystem§
MV,804,§Anwesenheit§ §Datenträger§ §fehlt§ §beim Schreiben§

MV,805,*************** §IFM-Identsystem§
MV,805,§Timeout§ §bei Beschreiben§ §Datenträger§

MV,806,*************** §IFM-Identsystem§
MV,806,§Gegenlesen§ §nach Beschreiben§ §fehlgeschlagen§

MV,807,*************** §IFM-Identsystem§
MV,807,§WT§-§Typ§ §falsch§ - §austauschen§
*)


(* ______________________ *)
(* -- Lokale Variablen -- *)
VAR PERSISTENT
	in						:FB_IfmDte101_20bytes_Data_In; (* Schnittstelle im Klartext für Kanal *)
	out						:FB_IfmDte101_20bytes_Data_Out; (* Schnittstelle im Klartext für Kanal *)

	n						:INT := 0;
	enteredByte				:BYTE;

	iWtNr					:INT;
	foundWtNr				:INT;

	found					:BOOL:=FALSE;
	FreeEntry				:INT; (* NummernVorschlag für WT *)
	_Uid_Empty  			:ARRAY[1..8] OF BYTE := [0,0,0,0,0,0,0,0];

	rtButtonCancel			:R_TRIG;

	stored_ibArUid			:ARRAY[1..8] OF BYTE := [0,0,0,0,0,0,0,0];
	tofFeSchreibenAwFehlt	:TOF;
END_VAR



]]></Declaration>
    <Implementation>
      <ST><![CDATA[(* _______________________ *)
(* -- Initialisierungen -- *)
IF Neustart THEN
	FOR n := 1 TO ErrMaxBits BY 1 DO
		Errors1[n].Txt := in_sBetriebsmittelname;
	END_FOR
END_IF


(* ___________________________ *)
(* -- Einlesen der Eingänge -- *)
(* Kanal 1 *)
in.ixTP			:=  iwRwhUid_20Bytes[1].0;	(* Transponder Present *)
in.ixAI			:=  iwRwhUid_20Bytes[1].1;	(* Antennna inactiv *)
in.ixWR			:=  iwRwhUid_20Bytes[1].2;	(* Schreiben Fertig *)
in.ixRD			:=  iwRwhUid_20Bytes[1].3;	(* Lesen Fertig *)
in.ixUD			:=  iwRwhUid_20Bytes[1].4;	(* Gibt an, ob Zugriff auf "User Speicher" Aktiv ist *)
in.ibRL			:=	iwRwhUid_20Bytes[2];	(* Länge der gelesenen Daten *)
in.ibArUid[1]	:=	iwRwhUid_20Bytes[3]; 	(* Gelesene Daten *)
in.ibArUid[2]	:=	iwRwhUid_20Bytes[4];
in.ibArUid[3]	:=	iwRwhUid_20Bytes[5];
in.ibArUid[4]	:=	iwRwhUid_20Bytes[6];
in.ibArUid[5]	:=	iwRwhUid_20Bytes[7];
in.ibArUid[6]	:=	iwRwhUid_20Bytes[8];
in.ibArUid[7]	:=	iwRwhUid_20Bytes[9];
in.ibArUid[8]	:=	iwRwhUid_20Bytes[10];


(* ____________________ *)
(* -- Timer / Zeiten -- *)
UZ(IN:= NOT SWI, PT:= T#5S);


(* ______________________ *)
(* -- Fehlerbehandlung -- *)
Fehler1( ID:= FaultID, FAnz:= DisplayNr, EF:= Errors1, F:= f1 );

Fault := FALSE	(* Baustein-Fehler *);
Warning := FALSE;
Waiting := FALSE;

FOR n := 1 TO ErrMaxBits BY 1 DO
	IF f1[n] THEN
		IF 		Errors1[n].Prio <= ErrZyl 			THEN Fault := TRUE;
		ELSIF	Errors1[n].Prio <= ErrEndl 			THEN Waiting := TRUE; f1[n] := FALSE;
		ELSIF	Errors1[n].Prio <= ErrWechselpos	THEN Warning := TRUE; f1[n] := FALSE; END_IF
	END_IF
END_FOR
SetFehlerM(Fault,	32,MoNr);
SetEndlageM(Waiting,32,MoNr);
SetWarnungM(Warning,32,MoNr);


(* P Hofmann 28 11 2014 *)
(* Einfügen dass IFM Lesekopf nicht arbeitet, wenn Anlage nicht läuft, um Anlauf für IFM zu erleichtern *)
(*
IF NOT BedienEinheit.anl_ein THEN
	Schritt:= 0;
END_IF
*)


(* _______________________________ *)
(* -- Stations - Betriebsmerker -- *)
Betrieb:= NOT Fault;


out.qxWR 		:= FALSE;
out.qxRD 		:= FALSE;
out.qxUR 		:= FALSE;

SWI 			:= FALSE;

out_bWriteDone 	:= FALSE;
out_bReadDone 	:= FALSE;
out_bIsWriting	:= (Schritt >= 50 AND Schritt < 100);

tofFeSchreibenAwFehlt( PT := T#4S );
tofFeSchreibenAwFehlt.IN := FALSE;
f1[feSchreibenAwFehlt] := tofFeSchreibenAwFehlt.Q;
Errors1[feSchreibenAwFehlt].Prio := ErrZentr;


rtButtonCancel(
	CLK :=
		(	(BDEStringBtnAnswer = 2)
		AND (BDEStringState <> 1)	)
	OR 	Neustart );


CASE Schritt OF
(* == Schritt  [         0]:
zyklisches Lesen + Sprungverteiler für Handshake zu Lesen und Schreiben == *)

0: (* Grundschritt *)
	IF Betrieb
	AND in_bChInUse
	AND BedienEinheit.notaus_ok THEN
		Schritt:= 20;
		SWI 	:= TRUE;
	END_IF;

20: (* Warten auf Anforderung Lesen/Schreiben *)
	out.qxUR 		:= TRUE;
	IF  NOT Fault AND Betrieb THEN
		IF in_bWriteStart THEN
			Schritt := 50;
			SWI 	:= TRUE;
		ELSIF in_bReadStart
		THEN
			Schritt := 100;
			SWI 	:= TRUE;
		END_IF
	END_IF


(*********************** Schreiben ****************************************)
50:	(* Freischritt *)
	Schritt := 60;
	SWI 	:= TRUE;

60: (* Warten ob Datenträger vorhanden ist *)
	IF uz.Q THEN
		tofFeSchreibenAwFehlt.IN := TRUE;
		Schritt := 0;
		SWI 	:= TRUE;

	ELSIF in.ixTP THEN
		stored_ibArUid := in.ibArUid;
		Schritt := 70;
		SWI 	:= TRUE;

	ELSE
		f1[feSchreibenAwFehlt] := TRUE;
		IF 	uz.ET < T#2S THEN
				Errors1[feSchreibenAwFehlt].Prio := ErrFw;
		ELSE	Errors1[feSchreibenAwFehlt].Prio := ErrEndl;
		END_IF
	END_IF


70: (* Prüfen ob UID bereits angelegt ist, wenn ja, dann löschen, und schon mal einen Freien Platz suchen *)

	(* alle Datensätze durchsuchen, ob UID des Datenträgers bereits für einen WT zugewiesen ist.
	wird die Kennung bei einem WT gefunden, so soll die UID bei dem gefundenen WT gelöscht werden
	(mit 0-Bytes überschreiben) *)
	FOR iWtNr:= 1 TO g_maxNumWt BY 1 DO
		(* suchen ob UID schon vergeben ist *)
		IF (MEMCMP(ADR(stored_ibArUid),ADR(g_WT_UID_Data[iWtNr]),SIZEOF(stored_ibArUid)) = 0) THEN
			(* Datenträger in Tabelle gefunden *)
			(* Platz leeren *)
			g_WT_UID_Data[iWtNr] := _Uid_Empty;
		END_IF
	END_FOR

	Schritt := Schritt + 3;
	SWI := TRUE;

73:	(* suche bei eingegebenem Typ den nächsten freien Platz *)
	FreeEntry:= 0;
	found:=FALSE;

	FOR iWtNr:= in_WtNrMin TO in_WtNrMax BY 1 DO (* bei WT-Typ 1 sind WT-Nummern [1..99] erlaubt *)
		(* Wenn noch kein freier Platz gefunden wurde *)
		IF NOT found THEN
			(* Prüfen ob Platz frei ist *)
			IF (MEMCMP(ADR(_Uid_Empty),ADR(g_WT_UID_Data[iWtNr]),SIZEOF(_Uid_Empty)) = 0) THEN
				(* Freier Platz in Tabelle gefunden *)
				found:= TRUE;
				FreeEntry:= iWtNr;
			END_IF
		END_IF
	END_FOR
	Schritt := 80;
	SWI := TRUE;



80:	IF rtButtonCancel.Q THEN
		Schritt := 0;
		SWI := TRUE;

	(* Eingabeaufforderung bei BDE-Online für die zu beschriftenden Daten *)
	(* Vergleichen ob Datenträger schon im Register eingetraten ist, Schreibebefehl kann ja direkt kommen *)
	ELSIF (BDEStringState=0) (* warte bis letzte Eingabeaufforderung geschlossen wurde *)
	THEN
		BDEStringState := 2;

		BDEStringDescr := '';
		BDEStringDescr := CONCAT( BDEStringDescr,	'§bei§:$n' 						);
		BDEStringDescr := CONCAT( BDEStringDescr, 	StatString						);
		BDEStringDescr := CONCAT( BDEStringDescr,	'$n$n§Lesekopf§:$n'				);
		BDEStringDescr := CONCAT( BDEStringDescr,	in_sBetriebsmittelname			);
		BDEStringDescr := CONCAT( BDEStringDescr,	'$n$n§WT-Nr.§ ('	);

		BDEStringDescr := CONCAT( BDEStringDescr,	INT_TO_STRING(in_WtNrMin) 	); (* WtNrMin *)
		BDEStringDescr := CONCAT( BDEStringDescr,	'-' 						);
		BDEStringDescr := CONCAT( BDEStringDescr,	INT_TO_STRING(in_WtNrMax) 	); (* WtNrMax *)


		BDEStringDescr := CONCAT( BDEStringDescr,	')$n§eigeben§:'					);

		IF (FreeEntry > 0) THEN
			BDEStringValue := INT_TO_STRING(FreeEntry); (* als Vorgabewert den ersten freien Platz vorschlagen *)
		END_IF
	END_IF
	Schritt := 90;
	SWI := TRUE;

90:	IF rtButtonCancel.Q THEN
		Schritt := 0;
		SWI := TRUE;

	(* Zu Schreibende Daten vom Benutzer abfragen *)
	ELSIF (BDEStringState=0) THEN (* warte bis letzte Eingabeaufforderung geschlossen wurde *)
		IF 	(LEN(BDEStringValue) < 8)
		AND (STRING_TO_INT(BDEStringValue) >= in_WtNrMin)
		AND (STRING_TO_INT(BDEStringValue) <= in_WtNrMax)
		THEN
			out_bWriteDone := TRUE;
			g_Wt_UID_Data[STRING_TO_INT(BDEStringValue)] := stored_ibArUid;
			
			// MEMCPY (ADR(g_WT_UID_Data[eTyp][STRING_TO_INT(BDEStringValue)]),ADR(stored_ibArUid),SIZEOF(stored_ibArUid));
			Schritt := 0;
			SWI := TRUE;
		ELSE
			(* Fehler - zum Beschreiben dürfen max. 8 Byte eingegeben werden *)
			(* -> erneute Eingabeaufforderung mit Hinweis auf max. Anzahl von Zeichen ausgeben *)
			BDEStringState := 2;

			BDEStringDescr := '';
			BDEStringDescr := CONCAT( BDEStringDescr,	'§bei§:$n' 							);
			BDEStringDescr := CONCAT( BDEStringDescr, 	StatString							);
			BDEStringDescr := CONCAT( BDEStringDescr,	'$n$n§Lesekopf§:$n'					);
			BDEStringDescr := CONCAT( BDEStringDescr,	in_sBetriebsmittelname				);
			BDEStringDescr := CONCAT( BDEStringDescr,	'$n$n§Werkstückträger-Nummer§ ('	);

			BDEStringDescr := CONCAT( BDEStringDescr,	INT_TO_STRING(in_WtNrMin)			); (* WtNrMin *)
			BDEStringDescr := CONCAT( BDEStringDescr,	'-' 								);
			BDEStringDescr := CONCAT( BDEStringDescr,	INT_TO_STRING(in_WtNrMax) 			); (* WtNrMax *)

			BDEStringDescr := CONCAT( BDEStringDescr,	')$n§eigeben§:'						);

			BDEStringValue := '';
			IF (FreeEntry > 0) THEN
				BDEStringValue := CONCAT( 	BDEStringValue, 	INT_TO_STRING(FreeEntry)	); (* als Vorgabewert den ersten freien Platz vorschlagen *)
				BDEStringValue := CONCAT( 	BDEStringValue, 	'? '						);
			END_IF
			BDEStringValue := CONCAT( BDEStringValue,	'('							 		);
			BDEStringValue := CONCAT( BDEStringDescr,	INT_TO_STRING(in_WtNrMin) 			); (* WtNrMin *)
			BDEStringValue := CONCAT( BDEStringDescr,	'-' 								);
			BDEStringValue := CONCAT( BDEStringDescr,	INT_TO_STRING(in_WtNrMax) 			); (* WtNrMax *)
	
			BDEStringValue := CONCAT( BDEStringValue,	' §eingeben§)'						);
		END_IF
	END_IF


(*********************** Lesen ********************************************)
100:IF NOT f1[feFalscherWtTypGefunden] (* warte bis Meldung quittiert *)
	THEN
		Schritt := 110;
		SWI := TRUE;
	END_IF

110: (* Warten ob Datenträger vorhanden ist *)
	IF uz.Q THEN
		(* Datenträger entfernt -> Ende *)
		f1[feLesenAwFehlt] := TRUE;
		Schritt := 0;
		SWI 		:= TRUE;
	ELSIF in.ixTP THEN (* Datenträger vorhanden *)

		IF memcmp( ADR(in.ibArUid), ADR(_Uid_Empty), SIZEOF(_Uid_Empty)) <> 0
		THEN
			(* not empty *)
			stored_ibArUid := in.ibArUid;

			(* Prüfen ob UID schon in Tabelle eingestragen ist *)
			foundWtNr	:= 0;
			FOR iWtNr := 1 TO g_maxNumWt BY 1 DO
				IF (MEMCMP(ADR(stored_ibArUid),ADR(g_WT_UID_Data[iWtNr]),SIZEOF(stored_ibArUid)) = 0) THEN
					(* Datenträger in Tabelle gefunden *)
					foundWtNr 	:= iWtNr;
					EXIT;
				END_IF
			END_FOR
				
			IF foundWtNr <> 0
			THEN
				(* Datenträger in Tabelle gefunden *)
				out_iUidRead 	:= foundWtNr;
		
				Schritt			:= 130;
				out_bReadDone	:= TRUE;
				SWI 			:= TRUE;
			ELSE
				(* Datenträger nicht in Tabelle gefunden *)
				(* -> Datensatz in Tabelle anlegen *)
				Schritt := 50;
				SWI := TRUE;
			END_IF
		END_IF
	END_IF

130: (* Warten auf Rückgang Leseanforderung *)
	out_bReadDone	:= TRUE;

	IF NOT in_bReadStart THEN
		Schritt:= 0;
		SWI 		:= TRUE;
	END_IF
END_CASE


(* ___________________________ *)
(* -- Ausgeben der Ausgänge -- *)
qwRwhUid_20Bytes[1] := 0;				(* Steuerbefehle *)
qwRwhUid_20Bytes[2] := out.qbRL;		(* Byte 2 - Wieviele Bytes sollen gelesen werden *)
qwRwhUid_20Bytes[3] := out.qbM1;		(* Byte 3 - Speicheradresse Teil 1 - Lesen/Schreiben *)
qwRwhUid_20Bytes[4] := out.qbM2;		(* Byte 4 - Speicheradresse Teil 2 - Lesen/Schreiben *)
qwRwhUid_20Bytes[5] := out.byteUid[1];	(* Byte 1 - User Data Area *)
qwRwhUid_20Bytes[6] := out.byteUid[2];	(* Byte 2 - User Data Area *)
qwRwhUid_20Bytes[7] := out.byteUid[3];	(* Byte 3 - User Data Area *)
qwRwhUid_20Bytes[8] := out.byteUid[4];	(* Byte 4 - User Data Area *)
qwRwhUid_20Bytes[9] := out.byteUid[5];	(* Byte 5 - User Data Area *)
qwRwhUid_20Bytes[10]:= out.byteUid[6];	(* Byte 6 - User Data Area *)
qwRwhUid_20Bytes[11]:= out.byteUid[7];	(* Byte 7 - User Data Area *)
qwRwhUid_20Bytes[12]:= out.byteUid[8];	(* Byte 8 - User Data Area *)
qwRwhUid_20Bytes[13] := 0;				(* Byte 2 - Digitalsignale (Bits setzen: siehe unten) *)
qwRwhUid_20Bytes[14] := 0;				(* Byte 2 - Digitalsignale (Bits setzen: siehe unten) *)
qwRwhUid_20Bytes[15] := 0;				(* Byte 2 - Digitalsignale (Bits setzen: siehe unten) *)
qwRwhUid_20Bytes[16] := 0;				(* Byte 2 - Digitalsignale (Bits setzen: siehe unten) *)
qwRwhUid_20Bytes[17] := 0;				(* Byte 2 - Digitalsignale (Bits setzen: siehe unten) *)
qwRwhUid_20Bytes[18] := 0;				(* Byte 2 - Digitalsignale (Bits setzen: siehe unten) *)
qwRwhUid_20Bytes[19] := 0;				(* Byte 2 - Digitalsignale (Bits setzen: siehe unten) *)
qwRwhUid_20Bytes[20] := 0;				(* Byte 2 - Digitalsignale (Bits setzen: siehe unten) *)


qwRwhUid_20Bytes[1].2	:=FALSE(*out.qxWR *);	(* Byte 1 - Bit 2 Starte Schreiben *)
qwRwhUid_20Bytes[1].3	:=out.qxRD;				(* Byte 1 - Bit 3 Starte Lesen *)
qwRwhUid_20Bytes[1].4	:=FALSE (*out.qxUR*);	(* Byte 1 - Bit 4 Zugriff auf User Data Area *)


















]]></ST>
    </Implementation>
    <LineIds Name="FB_IfmDte101">
      <LineId Id="1406" Count="30" />
      <LineId Id="1438" Count="44" />
      <LineId Id="2156" Count="0" />
      <LineId Id="2155" Count="0" />
      <LineId Id="2157" Count="0" />
      <LineId Id="2159" Count="0" />
      <LineId Id="2161" Count="0" />
      <LineId Id="2160" Count="0" />
      <LineId Id="1483" Count="20" />
      <LineId Id="1505" Count="16" />
      <LineId Id="1523" Count="2" />
      <LineId Id="2158" Count="0" />
      <LineId Id="1527" Count="1" />
      <LineId Id="2173" Count="0" />
      <LineId Id="1529" Count="0" />
      <LineId Id="1937" Count="0" />
      <LineId Id="1530" Count="1" />
      <LineId Id="2164" Count="0" />
      <LineId Id="2163" Count="0" />
      <LineId Id="2166" Count="0" />
      <LineId Id="2168" Count="0" />
      <LineId Id="2165" Count="0" />
      <LineId Id="2170" Count="1" />
      <LineId Id="1532" Count="133" />
      <LineId Id="1943" Count="0" />
      <LineId Id="1945" Count="2" />
      <LineId Id="1938" Count="0" />
      <LineId Id="1953" Count="10" />
      <LineId Id="1965" Count="12" />
      <LineId Id="1952" Count="0" />
      <LineId Id="1948" Count="0" />
      <LineId Id="1668" Count="0" />
      <LineId Id="1697" Count="7" />
      <LineId Id="1706" Count="0" />
      <LineId Id="1709" Count="46" />
      <LineId Id="9" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>